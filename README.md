assume we have csv files:
    user_data.csv
    image_metadata.csv
    *latest version is under ~/arradata_master
then run
    src/risk_predictor/create_label_dic.py
to create
    data_sid.pickle
    data_label.pickle
    data_caption.pickle
    data_comment.pickle
    data_split.pickle
    caption_data.txt
    comment_data.txt
put
    caption_data.txt
    comment_data.txt
    wikipedia dump data
into a folder and zip it.
then run
    create_word2vec_embeddings.py
to generate word2vec dictionary.
Now ready to run train_revised.py