from dataset import get_train, TextDataset
from utils import load_wvmap
wvmap = load_wvmap('vecDict_1013.pickle')
datapath = 'tobacco/test'
train_loader = get_train(datapath, 4, wvmap)
i, batch = next(enumerate(train_loader))
print(batch)
