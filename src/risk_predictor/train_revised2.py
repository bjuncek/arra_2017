import torch
from torch import nn, cat, LongTensor
from torch.nn import functional as F
from torch.nn import L1Loss, MSELoss, Linear, Dropout, Sequential, CrossEntropyLoss
from torch.nn.utils import clip_grad_norm  # cliping not needed if use weight decay
from torch.utils.data import DataLoader
from torch.autograd import Variable
from torch.optim import Adam
from torchtext import datasets, data
from torchtext.data import Field

import argparse
from os import makedirs
from os.path import join, isfile, exists, dirname
import pickle

from utils import load_wvmap, vectorize, generate_dicts
#from dataset import TextDataset, MediaDataset
from dataset_revised import MediaDataset

from models import create_resnet, LSTMModel
from report import Report

import sys
sys.path.append('..')
from shared.utils import set_all_seeds, SnapshotManager, ETC
from shared.lr_scheduler import ReduceLROnPlateau

TOPICS = ['breakup',
          'creativity',
          'death_love',
          'drinks',
          'food',
          'happy',
          'illegal_drugs',
          'major_friendship',
          'narcissist',
          'other_loss',
          'prescription_drugs',
          'tobacco']  # 11 topics


TOPICS_HELP = """index of target topic
survey_tobacco: 11
survey_illegal_drugs: 6
survey_prescription_drugs: 10
survey_drinks: 3
"""


def train(argv=sys.argv[1:]):
    print(argv)
    parser = argparse.ArgumentParser()
    parser.add_argument('--snapshot_dir', '-s',
                        type=str,
                        help='path to save snapshots of models')
    parser.add_argument('--topic', '-t',
                        type=int,
                        help=TOPICS_HELP)
    parser.add_argument('--lr', '-l',
                        type=float,
                        default=1e-2,
                        help='initial learning rate')
    parser.add_argument('--binarize', '-b',
                        action='store_true',
                        default=False)
    parser.add_argument('--on_server',
                        action='store_true',
                        default=False)
    parser.add_argument('--nhid_factor',
                        type=int,
                        default=1)
    parser.add_argument('--epochs',
                        type=int,
                        default=400)
    parser.add_argument('--ratio',
                        nargs="+",
                        type=int,
                        default=[20, 20, 40])
    args = parser.parse_args(argv)

    """
    E2E Training script for image, captions and comments
        by aggregating features with another LSTM
    """
    on_server = args.on_server
    topic = TOPICS[args.topic]
    print("Selected topic: {}".format(topic))
    snapshot_dir = args.snapshot_dir
    num_classes = 2 if args.binarize else 5

    if on_server:
        src_path = '/pool1/users/ntomita/arra_2017/src/risk_predictor'
        image_path = '/pool1/users/ntomita/arradata_images_original/arra'
    else:
        src_path = '/home/ntomita/arra_2017/src/risk_predictor'
        image_path = '/home/ntomita/arradata_images_original/arra'
    mapping_path = join(src_path, 'vec_dict_0123.pickle')
    sids_path = join(src_path, 'data_sid.pickle')
    label_dic_path = join(src_path, 'data_label.pickle')
    split_dic_path = join(src_path, 'data_split.pickle')
    caption_dic_path = join(src_path, 'data_caption.pickle')
    comment_dic_path = join(src_path, 'data_comment.pickle')

    ratio = args.ratio  # [20, 20, 40]

    makedirs(snapshot_dir, exist_ok=True)

    mapping = load_wvmap(mapping_path)

    sids = pickle.load(open(sids_path, 'rb'))
    label_dic = pickle.load(open(label_dic_path, 'rb'))
    split_dic = pickle.load(open(split_dic_path, 'rb'))
    caption_dic = pickle.load(open(caption_dic_path, 'rb'))
    comment_dic = pickle.load(open(comment_dic_path, 'rb'))

    topic = 'survey_' + topic

    train_dataloader = MediaDataset(sids,
                                    label_dic,
                                    split_dic,
                                    image_path,
                                    caption_dic,
                                    comment_dic,
                                    ratio,
                                    mapping,
                                    topic,
                                    mode='train',
                                    binarize=args.binarize)
    val_dataloader = MediaDataset(sids,
                                  label_dic,
                                  split_dic,
                                  image_path,
                                  caption_dic,
                                  comment_dic,
                                  ratio,
                                  mapping,
                                  topic,
                                  mode='val',
                                  binarize=args.binarize)

    train_dataloader.fix_imbalance()
    val_dataloader.fix_imbalance()

    d_embed = 300
    out_size = 300
    cuda = True

    """ net0: resnet (image feature extraction)
        net1: lstm (text feature extraction)
        net2: lstm (aggregation of image and text features)
    """
    net0 = create_resnet()
    fnhid = args.nhid_factor
    net1 = LSTMModel(nlayers=1, nhid=128*fnhid, in_size=d_embed,
                     out_size=out_size, need_unpack=True)
    # net2 = nn.Sequential(nn.ReLU(),
    #                      nn.Linear(out_size, num_classes))
    net2 = nn.Sequential(nn.ReLU(),
                         nn.Linear(out_size, 3*out_size),
                         nn.Dropout(p=0.5),
                         nn.ReLU(),
                         nn.Linear(3*out_size, num_classes))

    criterion = CrossEntropyLoss()

    if cuda:
        net0 = net0.cuda()
        net1 = net1.cuda()
        net2 = net2.cuda()
        criterion = criterion.cuda()

    params = list(net0.parameters()) +\
        list(net1.parameters()) +\
        list(net2.parameters())

    optimizer = Adam(params, lr=args.lr, weight_decay=1e-5)
    scheduler = ReduceLROnPlateau(optimizer,
                                  mode='min',
                                  factor=0.5,
                                  verbose=1,
                                  patience=20,
                                  cooldown=10)

    num_epochs = args.epochs
    timer = ETC(num_epochs)
    snapshot_manager = SnapshotManager(
        mode='min', every=10, ignore_first=10)

    def train_epoch(epoch):
        net0.train(True)
        net1.train(True)
        net2.train(True)
        epoch_loss_obj = 0.
        reporter = Report(num_classes=num_classes)
        #for i, batch in enumerate(train_dataloader, 1):
        for i in range(len(train_dataloader)):
            batch = train_dataloader[i]
            images, caps, coms, label = batch
            label = label.type(LongTensor)
            images = Variable(images)
            h0_cap = net1.init_hidden(ratio[1])
            h0_com = net1.init_hidden(ratio[2])
            if cuda:
                images = images.cuda()
                label = label.cuda()
                h0_cap = (h0_cap[0].cuda(), h0_cap[1].cuda())
                h0_com = (h0_com[0].cuda(), h0_com[1].cuda())
            optimizer.zero_grad()

            feat_imgs = net0(images)
            feat_caps = net1(caps, h0_cap)
            feat_coms = net1(coms, h0_com)

            feats = cat([feat_imgs, feat_caps[-1], feat_coms[-1]], 0)
            feats = feats.view(feats.size(0), -1)
            feats = torch.mean(feats, dim=0)
            pred = net2(feats).view(1, -1)
            loss = criterion(pred, label)
            reporter.feed_softmax(pred, label)

            epoch_loss_obj += loss.data[0]
            loss.backward()
            optimizer.step()
            del images, feat_imgs, feat_caps, feat_coms, feats
            del h0_cap, h0_com
            del pred, loss
        avg_loss = epoch_loss_obj / len(train_dataloader)
        print("\nEpoch {} : Avg Loss: {:.4f}".format(epoch, avg_loss))
        print(reporter)
        print(reporter.show_stats())

    def validate(epoch):
        net0.train(False)
        net1.train(False)
        net2.train(False)
        epoch_loss_obj = 0.
        reporter = Report(num_classes=num_classes)
        for i in range(len(val_dataloader)):
            batch = val_dataloader[i]
            images, caps, coms, label = batch
            label = label.type(LongTensor)
            images = Variable(images)
            h0_cap = net1.init_hidden(ratio[1])
            h0_com = net1.init_hidden(ratio[2])
            if cuda:
                images = images.cuda()
                label = label.cuda()
                h0_cap = (h0_cap[0].cuda(), h0_cap[1].cuda())
                h0_com = (h0_com[0].cuda(), h0_com[1].cuda())
            feat_imgs = net0(images)
            feat_caps = net1(caps, h0_cap)
            feat_coms = net1(coms, h0_com)
            feats = cat([feat_imgs, feat_caps[-1], feat_coms[-1]], 0)
            feats = feats.view(feats.size(0), -1)
            feats = torch.mean(feats, dim=0)
            pred = net2(feats).view(1, -1)
            loss = criterion(pred, label)
            reporter.feed_softmax(pred, label)

            epoch_loss_obj += loss.data[0]
            del images, feat_imgs, feat_caps, feat_coms, feats
            del h0_cap, h0_com
            del pred, loss
        avg_loss = epoch_loss_obj / len(val_dataloader)
        print("\n==> Avg Loss: {:.4f}".format(avg_loss))
        print(reporter)
        scheduler.step(avg_loss, epoch)
        if snapshot_manager.need_save(avg_loss, epoch):
            torch.save(net0, join(snapshot_dir, "net0-{}.pth".format(epoch)))
            torch.save(net1, join(snapshot_dir, "net1-{}.pth".format(epoch)))
            torch.save(net2, join(snapshot_dir, "net2-{}.pth".format(epoch)))

    for epoch in range(1, num_epochs+1):
        sys.stdout.flush()
        train_epoch(epoch)
        validate(epoch)
        timer.tick()


if __name__ == '__main__':
    train()
