import torch
import sys


class Report:

    def __init__(self, num_classes=5):
        self.counts = [sys.float_info.epsilon for x in range(num_classes)]
        self.success = [0. for x in range(num_classes)]
        self.num_classes = num_classes

    def feed(self, pred, label):
        pred = pred.squeeze().cpu()
        label = label.squeeze().cpu()
        assert pred.numel() == label.numel()
        for i in range(pred.numel()):
            target = int(label.data[i])
            self.counts[target] += 1
            l_bound = target - 1/2
            r_bound = target + 1/2
            if l_bound < 0:
                l_bound = -float('inf')
            if r_bound > (self.num_classes - 1):
                r_bound = float('inf')
            p = float(pred.data[i])
            if l_bound < p and p < r_bound:
                self.success[target] += 1

    def feed_softmax(self, pred, label):
        pred = pred.cpu()
        _, pred = torch.max(pred, 1)
        label = label.squeeze().cpu()
        assert pred.numel() == label.numel()
        local_success_count = 0
        for i in range(pred.numel()):
            target = int(label.data[i])
            self.counts[target] += 1
            if target == int(pred.data[i]):
                self.success[target] += 1
                local_success_count += 1
        return local_success_count / (pred.numel() + sys.float_info.epsilon)

    def accuracy(self):
        return [self.success[i]/self.counts[i] for i in range(self.num_classes)]

    def show_stats(self):
        stats = ["Class {} counts: {}".format(i, int(self.counts[i])) for i in range(self.num_classes)]
        stats += ["Class {} success: {}".format(i, int(self.success[i])) for i in range(self.num_classes)]
        return '\n'.join(stats)

    def __str__(self):
        summary = ["Class {} accuracy: {:.4f}".format(i, self.success[i]/self.counts[i]) for i in range(self.num_classes)]
        avg = sum([self.success[i]/self.counts[i] for i in range(self.num_classes)]) / self.num_classes
        summary.append("Average accuracy over classes: {:.4f}".format(avg))
        return '\n'.join(summary)
