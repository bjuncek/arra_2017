from pandas import read_csv, isnull
import pickle
import numpy
from statistics import mean, stdev
from collections import Counter
from itertools import product

topics = ['survey_drinks',
          'survey_tobacco',
          'survey_illegal_drugs',
          'survey_prescription_drugs']

path_sids = 'data_sid.pickle'
path_user_data = '/home/ntomita/arradata_master/arra_user_data.csv'
sids_valid = pickle.load(open(path_sids, 'rb'))
user_dic = read_csv(path_user_data, engine='python')

users = user_dic.values.tolist()
users_clean = list()
for e in users:
    if e[2] in sids_valid:
        users_clean.append(e)
#print(len(users_clean))

# avg age
population = [e[3] for e in users_clean]
avg_age = mean(population)
std_age = stdev(population)
print("Avg:{:.1f}".format(avg_age), "Std:{:.1f}".format(std_age))

# drink people
drink_i, tobacco_i, illegal_i, presc_i = [8, 9, 10, 11]
age_i, gender_i, race_i, media_i, likes_i, follows_i, followed_i = [3, 4, 5, 12, 13, 15, 16]
topic_name = {8: "drink_i", 9: "tobacco_i", 10: "illegal_i", 11: "presc_i"}
data_name = {3: "age_i", 4: "gender_i", 5: "race_i", 12: "media_i", 13: "likes_i", 15: "follows_i", 16: "followed_i"}

configs = product([drink_i, tobacco_i, illegal_i, presc_i], [race_i, ])
for topic, data in configs:
    print(topic_name[topic], data_name[data])
    for i in range(1, 5+1):
        count = Counter()
        population = [e[data] for e in users_clean if e[topic] == i]
        if data == age_i:
            avg_ = mean(population)
            std_ = stdev(population)
            print("Scale:{}".format(i), "Avg:{:.1f}".format(avg_), "Std:{:.1f}".format(std_))
        elif data in [gender_i, race_i]:
            for e in population:
                count[e] += 1
            print("Scale:{}".format(i), "Total:{}".format(sum(count.values())), count)


    # # binary
    # for class_set in ([1,], [2,3,4,5]):
    #     population = [e[data] for e in users_clean if e[topic] in class_set]
    #     avg_ = mean(population)
    #     std_ = stdev(population)
    #     print("Scale:{}".format(class_set), "Avg:{:.1f}".format(avg_), "Std:{:.1f}".format(std_))
    print("-"*10)
