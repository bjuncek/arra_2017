Too many similar files:


* Test

testscript.py:
    some random script to print sample data
test.py:
    early pipeline build, use net0,net1,net2 in total
test2.py:
    upgrade test.py by using separate lstm models for caption and comment data (test.py shared one analyzer for both data)
test_revised2.py:
    Upgrade test.py by averaging features rather than using a aggregating module.
test_ablation.py:
    Upgrade test_revised2.py by allowing having a null input for some data modality. Made for ablation study but usable for general testing. (Final pipeline)


* Train

train.py
    initial implementation
train2.py
    Upgrade train.py by adopting separate sentence parser (corresponds to test2.py)
train_revised.py
    Upgrade train.py by using dataset_revised.py (changed how to feed data)
train_revised2.py
    Upgrade train_revised.py by adopting averaging strategy (corresponds to test_revised2.py)
train_ablation.py
    Modify train_revised2.py to train entire models with single source (such as only image, caption, or comment)
train_resnet.py
    This came after train_ablation.py. Probably, this trains image analyzer only in the context of a risk prediction, which uses feature aggregation (not like in a regular pretraining, which makes a prediction by looking at a single image (doesn't make sense now!))
pretrain.py
    This serves for train2.py by training text analyzers for each caption and commment data beforehand.

