import pickle
from os import walk
from os.path import join

def count_images(root):
    n_images = 0
    for dirpath, dirs, files in walk(root):
        for file in files:
            if file.startswith('.'):
                continue
            else:
                n_images += 1
    return n_images

sids = pickle.load(open('data_sid.pickle', 'rb'))
image_path = '/home/ntomita/arradata_images_original/arra'
for sid in sids:
    if count_images(join(image_path, str(sid))) == 0:
        print(sid)
