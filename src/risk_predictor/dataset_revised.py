import torch
from torch import cat, stack, FloatTensor
from torch.autograd import Variable
from torch.utils.data import Dataset
from torch.nn.utils.rnn import pack_padded_sequence
from torchvision.transforms import Compose, RandomCrop, ToTensor, RandomHorizontalFlip, Normalize, TenCrop, FiveCrop, Lambda

from random import choices, shuffle, sample, random
from os import listdir, walk
from os.path import join, dirname, basename
import sys
from collections import defaultdict

from PIL import Image, ImageFile
from utils import generate_dicts, vectorize
from pad import pad

sys.path.append('..')
from shared.transforms import Resize
#from shared.dataset_helper import compute_mean_ImageNet

ImageFile.LOAD_TRUNCATED_IMAGES = True
IMAGENET_MEAN = [0.485, 0.456, 0.406]
IMAGENET_STD = [0.229, 0.224, 0.225]

ARRA_MEAN = [0.50660007, 0.4513821, 0.42786052]
ARRA_STD = [0.26140054, 0.25699114, 0.23560474]


def transform_ImageNet(normalize=False, test=False,
                       normalize_mean=IMAGENET_MEAN,
                       normalize_std=IMAGENET_STD):
    """Based on Resnet paper
    """
    size = 256 if random() > 0.5 else 480
    transforms = [Resize(size=size),
                  RandomHorizontalFlip(),
                  RandomCrop(size=224),
                  ToTensor()]
    if normalize:
        transforms.append(Normalize(mean=normalize_mean,
                                    std=normalize_std))
    return Compose(transforms)


def transform_ImageNet_Test(normalize=False, size=224,
                            normalize_mean=IMAGENET_MEAN,
                            normalize_std=IMAGENET_STD):
    # ResNet paper uses different scale from [224, 256, 384, 480, 640]
    # and averages classification results
    normalize_t = Normalize(mean=normalize_mean,
                            std=normalize_std)

    transforms = [Resize(size=size),
                  TenCrop(224),
                  Lambda(lambda crops: torch.stack([normalize_t(ToTensor()(crop)) if normalize else ToTensor()(crop) for crop in crops]))]
    return Compose(transforms)


def get_label(dic, topic):
    return FloatTensor(1, 1, 1, 1).fill_(int(dic[topic]))


def get_images(path, n_samples=1):
    images_path = []
    for dirpath, dirs, files in walk(path):
        for file in files:
            images_path.append(join(path, file))
    return choices(images_path, k=n_samples), images_path


"""
opts.datapath
opts.n_samples
opts.mapping
"""


# assumption:
# datapath structure is
# /train/survey_id/[images]
# /val/survey_id/[images]
# /test/survey_id/[images]


class ImageDataset(Dataset):
    """ Random sampling over all image samples
        Use this for training CNN
    """
    def __init__(self, sids, label_dic, split_dic, image_path, nums_samples,
                 topic, cuda=True):
        mode = 'train'
        binarize = True
        self.sids = [sid for sid in sids if split_dic[sid] == mode]
        if mode == 'train':
            shuffle(self.sids)
        self.label_dic = label_dic
        self.split_dic = split_dic
        self.image_path = image_path
        self.nums_samples = nums_samples
        self.topic = topic
        self.mode = mode
        self.cuda = cuda
        self.binarize = binarize
        self.transform = transform_ImageNet(
            normalize=True,
            normalize_mean=ARRA_MEAN,
            normalize_std=ARRA_STD)
        #self.set_pools()

    def get_images(self, index):
        if self.nums_samples[0] == 0:
            return None
        key = self.sids[index]
        label = int(self.label_dic[key][self.topic]) - 1
        if label > 0:
            target_set = self.pos_images
        else:
            target_set = self.neg_images
        images = choices(target_set, k=self.nums_samples[0])
        inputs = list()

        for image in images:
            img = Image.open(image)
            if img.mode != 'RGB':
                # IRREGULAR INPUT
                img = img.convert(mode='RGB')
            ch = self.transform(img)
            inputs.append(ch)
        return torch.stack(inputs)

    def __getitem__(self, index):
        try:
            images = self.get_images(index)
        except IndexError:
            print(self.sids[index])
            exit()
        key = self.sids[index]
        label = int(self.label_dic[key][self.topic]) - 1  # force 0-indexing
        if self.binarize:
            label = 1 if label > 0 else 0
        label = Variable(FloatTensor(1).fill_(label))
        if self.cuda:
            label = label.cuda()
        # print(key)  # added for presentation purpose
        return images, label

    def __len__(self):
        return len(self.sids)

    def set_pools(self):
        self.pos_images = list()
        self.neg_images = list()
        for key in self.sids:
            path = join(self.image_path, str(key))
            for dirpath, dirs, files in walk(path):
                for file in files:
                    label = int(self.label_dic[key][self.topic]) - 1
                    if label > 0:
                        self.pos_images.append(join(path, file))
                    else:
                        self.neg_images.append(join(path, file))

    def fix_imbalance(self):
        """ Fix the possible class imbalance problem in dataset
            by oversampling smaller classes
            This function should be called after checking consistency
            of the data (esp. uid) across image, caption, and comment
            dataset.
        """
        bins = defaultdict(list)
        for key in self.sids:
            try:
                key_class = int(self.label_dic[key][self.topic]) - 1  # force 0-indexing
                if self.binarize and key_class > 0:
                    key_class = 1
                bins[key_class].append(key)
            except:
                continue
        largest_size = max([len(bins[k]) for k in bins.keys()])
        print("Largest size:", largest_size)
        keys = sorted(list(bins.keys()))
        for k in keys:
            print("Class {}:".format(k), len(bins[k]))
        new_db_keys = []
        for k in keys:
            new_db_keys += bins[k]  # guarantee all the elements appear at least once
            n_extra_samples = largest_size - len(bins[k])
            new_db_keys += choices(bins[k], k=n_extra_samples)
        shuffle(new_db_keys)
        print("Fixed Total:", len(new_db_keys))
        self.sids = new_db_keys
        self.set_pools()


class MediaDataset(Dataset):
    """A multimedia dataset that holds different dataloader inside and
    provide a set of media data for a user at a time
    For Arra project, this will have three dataloaders:
        images
        captions
        comments
    """
    def __init__(self, sids, label_dic, split_dic,
                 image_path, caption_dic, comment_dic, nums_samples,
                 mapping, topic, mode='train', cuda=True, binarize=False,
                 normalize_mean=IMAGENET_MEAN, normalize_std=IMAGENET_STD):
        self.sids = [sid for sid in sids if split_dic[sid] == mode]
        if mode == 'train':
            shuffle(self.sids)
        self.label_dic = label_dic
        self.split_dic = split_dic
        self.image_path = image_path
        self.caption_dic = caption_dic
        self.comment_dic = comment_dic
        self.nums_samples = nums_samples
        self.mapping = mapping
        self.topic = topic
        self.mode = mode
        self.cuda = cuda
        self.binarize = binarize
        self.transform = transform_ImageNet(
            normalize=True,
            normalize_mean=normalize_mean,
            normalize_std=normalize_std)

    def get_texts(self, index, type):
        key = self.sids[index]
        if type == 'caption':
            if self.nums_samples[1] == 0:
                return None
            population = [t for t in self.caption_dic[key] if t is not None]
            if len(population) == 0:
                population = [None,]
            population = choices(population, k=self.nums_samples[1])
        elif type == 'comment':
            if self.nums_samples[2] == 0:
                return None
            population = [t.split('||') for t in self.comment_dic[key] if t is not None]
            population = [t for ts in population for t in ts]
            if len(population) == 0:
                population = [None,]
            population = choices(population, k=self.nums_samples[2])
        sentencev = [vectorize(self.mapping, s) for s in population]
        try:
            seq_lens = [v.size(0) for v in sentencev]
        except RuntimeError:
            print(key, population)
            print(sentencev)
            exit()
        d = dict(zip(sentencev, seq_lens))
        # Sort sentence vectors by the length in decreasing order
        # (requirement of pack_padded_sequence)
        sentencev = sorted(sentencev, key=lambda x: d[x], reverse=True)
        sentencev = [x.view(x.size(1), x.size(0)) for x in sentencev]
        seq_lens = sorted(seq_lens, reverse=True)
        max_len = max(seq_lens)
        sentencev = [pad(v, (0, max_len-v.size(1)), 'constant', 0) for v in sentencev]
        sentencev = stack(sentencev, 0).view(max_len, len(seq_lens), -1)
        if self.cuda:
            sentencev = sentencev.cuda()
        return pack_padded_sequence(sentencev, seq_lens)

    def get_images(self, index):
        if self.nums_samples[0] == 0:
            return None
        key = self.sids[index]
        images, paths = get_images(join(self.image_path, str(key)), n_samples=self.nums_samples[0])
        inputs = list()

        for image in images:
            while True:
                try:
                    img = Image.open(image)
                    break
                except OSError:
                    print("Broken file (OSEr): ", image)
                    image = sample(paths, k=1)[0]
                except AttributeError:
                    print("Broken file (AtrEr): ", image)
                    image = sample(paths, k=1)[0]
            if img.mode != 'RGB':
                # IRREGULAR INPUT
                img = img.convert(mode='RGB')
            ch = self.transform(img)
            inputs.append(ch)
        return torch.stack(inputs)

    def get_images_test(self, index):
        # returns multi-resolution 10crop for n_samples
        # Returning tensor should be TxNxCxHxW where T is n_samples and N is multires 10crops
        if self.nums_samples[0] == 0:
            return None
        key = self.sids[index]
        images, paths = get_images(join(self.image_path, str(key)), n_samples=self.nums_samples[0])
        inputs = []

        for image in images:
            single_image_set = list()
            for size in [224, 256, 384, 480, 640]:
                transform = transform_ImageNet_Test(normalize=True, size=size)
                while True:
                    try:
                        img = Image.open(image)
                        break
                    except OSError:
                        print("Broken file (OSEr): ", image)
                        image = sample(paths, k=1)[0]
                    except AttributeError:
                        print("Broken file (AtrEr): ", image)
                        image = sample(paths, k=1)[0]
                if img.mode != 'RGB':
                    # IRREGULAR INPUT
                    img = img.convert(mode='RGB')
                ch = transform(img)
                single_image_set.append(ch)
            stacked = torch.stack(single_image_set)  # 5x10xCxHxW
            stacked = stacked.view(-1, stacked.size(2), stacked.size(3), stacked.size(4))
            inputs.append(stacked)
        return torch.stack(inputs)  # Tx50xCxHxW

    def __getitem__(self, index):
        try:
            images = self.get_images(index)
        except IndexError:
            print(self.sids[index])
            exit()
        caps = self.get_texts(index, 'caption')
        coms = self.get_texts(index, 'comment')
        key = self.sids[index]
        label = int(self.label_dic[key][self.topic]) - 1  # force 0-indexing
        if self.binarize:
            label = 1 if label > 0 else 0
        label = Variable(FloatTensor(1).fill_(label))
        if self.cuda:
            label = label.cuda()
        # print(key)  # added for presentation purpose
        return images, caps, coms, label

    def __len__(self):
        return len(self.sids)

    def stats(self):
        bins = defaultdict(list)
        for key in self.sids:
            try:
                key_class = int(self.label_dic[key][self.topic]) - 1  # force 0-indexing
                if self.binarize and key_class > 0:
                    key_class = 1
                bins[key_class].append(key)
            except:
                continue
        for k in sorted(list(bins.keys())):
            print("Class {}:".format(k), len(bins[k]))

    def fix_imbalance(self):
        """ Fix the possible class imbalance problem in dataset
            by oversampling smaller classes
            This function should be called after checking consistency
            of the data (esp. uid) across image, caption, and comment
            dataset.
        """
        bins = defaultdict(list)
        for key in self.sids:
            try:
                key_class = int(self.label_dic[key][self.topic]) - 1  # force 0-indexing
                if self.binarize and key_class > 0:
                    key_class = 1
                bins[key_class].append(key)
            except:
                continue
        largest_size = max([len(bins[k]) for k in bins.keys()])
        print("Largest size:", largest_size)
        keys = sorted(list(bins.keys()))
        for k in keys:
            print("Class {}:".format(k), len(bins[k]))
        new_db_keys = []
        for k in keys:
            new_db_keys += bins[k]  # guarantee all the elements appear at least once
            n_extra_samples = largest_size - len(bins[k])
            new_db_keys += choices(bins[k], k=n_extra_samples)
        shuffle(new_db_keys)
        print("Fixed Total:", len(new_db_keys))
        self.sids = new_db_keys

