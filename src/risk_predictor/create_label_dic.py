from pandas import read_csv, isnull
import pickle
from random import shuffle
import numpy

topics = ['survey_drinks',
          'survey_tobacco',
          'survey_illegal_drugs',
          'survey_prescription_drugs']
#topics_ind = list(range(7, 7+len(topics)+1))
topics_ind = [8 + i for i in range(len(topics))]
#path_user_data = '/home/ntomita/arradata/user_data.csv'
path_user_data = '/home/ntomita/arradata_master/arra_user_data.csv'

#path_image_metadata = '/home/ntomita/arradata/image_metadata.csv'
path_image_metadata = '/home/ntomita/arradata_master/clean_image_metadata.csv'

user_dic = read_csv(path_user_data, engine='python')
img_dic = read_csv(path_image_metadata, engine='python')
joint_dic = user_dic.merge(img_dic, on='survey_id')

label_dic = dict()
captions_dic = dict()
comments_dic = dict()

exclude = [4329, 4012]
sids = list(set(joint_dic['survey_id'].values.tolist()))
sids = [i for i in sids if int(i) not in exclude]
n_sids = len(sids)
print("# unique survey_id: ", n_sids)
n_train = int(n_sids*0.8)
n_test = int(n_sids*0.1)
n_val = n_sids - (n_train + n_test)
shuffle(sids)
set_train = sids[:n_train]
set_test = sids[n_train:n_train+n_test]
set_val = sids[n_train+n_test:]
assert n_train == len(set_train)
assert n_test == len(set_test)
assert n_val == len(set_val)
set_dic = dict()
for e in set_train:
    set_dic[e] = 'train'
for e in set_test:
    set_dic[e] = 'test'
for e in set_val:
    set_dic[e] = 'val'

CAPS_INDEX = 38  # COL INDEX OF CAPTION IN JOINT_DIC
COMS_INDEX = 37  # COL INDEX OF COMMENT IN JOINT_DIC


for sid in sids:
    indices = user_dic['survey_id'] == sid
    col = user_dic.loc[indices].values.tolist()[0]
    label_dic[sid] = dict()
    for i, topic in enumerate(topics):
        label_dic[sid][topic] = col[topics_ind[i]]

    indices = joint_dic['survey_id'] == sid
    cols = joint_dic.loc[indices].values.tolist()
    captions_dic[sid] = list()
    comments_dic[sid] = list()

    for col in cols:
        caption = col[CAPS_INDEX]
        comment = col[COMS_INDEX]
        if isnull(caption):
            caption = None
        if isnull(comment):
            comment = None
        captions_dic[sid].append(caption)
        comments_dic[sid].append(comment)

# a list of sids: [sid,]
pickle.dump(sids, open('data_sid.pickle', 'wb'))
# a dictionary of dictionary: {sid: {topic: label}}
pickle.dump(label_dic, open('data_label.pickle', 'wb'))
# a dictionary of caption data: {sid: [caption,]}
pickle.dump(captions_dic, open('data_caption.pickle', 'wb'))
# a dictioanry of comment data: {sid: [comment,]}
pickle.dump(comments_dic, open('data_comment.pickle', 'wb'))
# a dictionary of data split: {sid: 'train'|'test'|'val'}
pickle.dump(set_dic, open('data_split.pickle', 'wb'))


def generate_text_file(sids, data_dict, save_path):
    f = open(save_path, 'w')
    for sid in sids:
        for s in data_dict[sid]:
            if s is not None:
                for ss in s.split('||'):
                    f.write(ss+'\n')
    f.close()

generate_text_file(sids, captions_dic, 'caption_data.txt')
generate_text_file(sids, comments_dic, 'comment_data.txt')
