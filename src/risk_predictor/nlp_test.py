import torch
from torch import nn
from torch.nn import functional as F
from torch.autograd import Variable
from torchtext import datasets, data
from torchtext.data import Field
from os import makedirs
from os.path import join, isfile
from models import BiLSTMModel


class Classifier(nn.Module):
    def __init__(self, n_embed, d_embed):
        super(Classifier, self).__init__()
        self.lstm = BiLSTMModel(nlayers=2, nhid=512, in_size=300, out_size=1)

    def forward(self, x):
        h = self.lstm.init_hidden(x.size(1))  # [seq_len, T, dim_emb]
        return self.lstm(x, h)


def main():
    vector_dir = '/Users/Naofumi/arradata_text'
    vector_cache = 'vectors.pth'
    train_path = '/Users/Naofumi/arradata_text/tobacco/test.txts'

    inputs = data.Field(lower=True)
    answers = Field(sequential=False)
    d_embed = 300
    dtrain, dval, dtest = datasets.SST.splits(inputs, answers, fine_grained=True)
    inputs.build_vocab(dtrain, dval, dtest)

    use_word_vector = False
    if isfile(join(vector_dir, vector_cache)):
        inputs.vocab.vectors = torch.load(join(vector_dir, vector_cache))
        use_word_vector = True
    else:
        inputs.vocab.load_vectors(wv_dir=vector_dir, wv_type='glove.42B', wv_dim=d_embed)

    n_embed = len(inputs.vocab)
    net = Classifier(n_embed, d_embed)
    if use_word_vector:
        net.embedding.weight.data = inputs.vocab.vectors

    f = open(train_path)
    n_test = 3
    for i in range(n_test):
        s = f.readline()
        print(s)
        s = inputs.preprocess(s)
        print(s)
        s = [[inputs.vocab.stoi[x] for x in s]]
        x = inputs.tensor_type(s)
        x = Variable(x)

        e = net(x)
        print(e[-1])





if __name__ == '__main__':
    main()
