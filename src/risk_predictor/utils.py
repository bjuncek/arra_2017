import os
from os import walk, remove
from os.path import basename, join, dirname, isfile
from collections import defaultdict
import torch
from torch import cat, nn
from torch import FloatTensor
from torch.autograd import Variable

import pickle
from math import sqrt


def weight_init(m):
    if isinstance(m, nn.Linear):
        fan_out, fan_in = m.weight.size()
        variance = sqrt(2.0/(fan_in + fan_out))
        m.weight.data.normal_(0.0, variance)


def remove_broken_images(path):
    from PIL import Image
    for dirpath, dirs, files in walk(path):
        for file in files:
            file_path = join(dirpath, file)
            try:
                Image.open(file_path)
            except:
                print(file_path)
                remove(file_path)


def vectorize(mapping, string, print_unk=False):
    """
    Input:
        string: a whole sentence (not just a word)
    Returns Variable of size (n_words, dim_emb)
    """
    t = None
    if string is None:
        t = FloatTensor(mapping['UNK'])
        t = t.view(1, t.numel())
    else:
        for word in string.split():
            if word == '':
                continue
            if word in mapping:
                wordv = FloatTensor(mapping[word])
            else:
                if print_unk:
                    print("{} is UNK".format(word))
                wordv = FloatTensor(mapping['UNK'])
            wordv = wordv.view(1, wordv.numel())
            if t is None:
                t = wordv
            else:
                t = cat([t, wordv], 0)
    if t is None:
        t = FloatTensor(mapping['UNK'])
        t = t.view(1, t.numel())
    return Variable(t)


def generate_dicts(file_path):
    """
    Create a pair of dictionaries
        {uid: list of texts}
        {uid: a label }
        from a dataset in plain text
    """
    f = open(file_path)
    texts = defaultdict(list)
    labels = defaultdict(list)
    for line in f:
        uid, label, sentence = line.split('\t')
        texts[uid].append(sentence)
        labels[uid].append(label)
    return [texts, labels]


def load_wvmap(dict_path):
    """
    Load a dictionary created with w2v script
        with keys repaired
    """
    d = pickle.load(open(dict_path, 'rb'), encoding='bytes')
    keys = list(d.keys())
    for k in keys:
        new_key = k
        try:
            new_key = k.decode('utf-8')
        except:
            try:
                new_key = k.decode('unicode_escape')
            except:
                pass
        if new_key != k:
            d[new_key] = d[k]
            del d[k]
    return d
