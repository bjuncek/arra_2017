from PIL import Image, ImageFile
from os import walk
from os.path import join, dirname, basename
import numpy
ImageFile.LOAD_TRUNCATED_IMAGES = True

"""
Script to compute channel-wise mean and stdev of image dataset.
"""

images = list()
target_dir = '/home/ntomita/arradata_images_original'
for dirpath, dirs, files in walk(target_dir):
    for file in files:
        if file.startswith('.'):
            continue
        path = join(dirpath, file)
        images.append(path)

total_mean = None
total_std = None
for image in images:
    img = Image.open(image).convert(mode='RGB')
    img_mean = numpy.mean(img, axis=(0, 1)) / 255
    img_std = numpy.std(img, axis=(0, 1)) / 255
    if total_mean is None:
        total_mean = img_mean
    else:
        total_mean += img_mean
    if total_std is None:
        total_std = img_std
    else:
        total_std += img_std
print("Mean:", total_mean / len(images))
print("Stdev:", total_std / len(images))
