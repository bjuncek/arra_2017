import torch
from torch import nn, cat, LongTensor
from torch.nn import functional as F
from torch.nn import L1Loss, MSELoss, Linear, Dropout, Sequential, CrossEntropyLoss
from torch.nn.utils import clip_grad_norm  # cliping not needed if use weight decay
from torch.utils.data import DataLoader
from torch.autograd import Variable
from torch.optim import Adam
from torchtext import datasets, data
from torchtext.data import Field

from os import makedirs
from os.path import join, isfile, exists, dirname
import pickle

from utils import load_wvmap, vectorize, generate_dicts
from dataset import TextDataset, MediaDataset
from models import create_resnet, BiLSTMModel
from report import Report

import sys
sys.path.append('..')
from shared.utils import set_all_seeds, SnapshotManager, ETC
from shared.lr_scheduler import ReduceLROnPlateau


""" This pretraining of text processing module might not
    work well, as with subtle signal of correct labeling
    each packpropagation of error doesn't effectively learn
    signals we need.
"""

def pretrain():
    """
    Pretrain seq-lstm using captions and comments data
    """
    snapshot_dir = '/home/ntomita/arra_textlstm_snapshot_1127'
    snapshot_dir = '/home/ntomita/arra_textlstm_snapshot_1128'
    if not exists(snapshot_dir):
        makedirs(snapshot_dir)

    images_path = None
    captions_path = '/home/ntomita/arradata_text/tobacco/train'
    comments_path = '/home/ntomita/arradata_comments/tobacco/train'
    ratio = [0, 10, 30]

    #mapping_path = '/home/ntomita/arradata_text/vec_dict_1017_capcom.pickle'
    mapping_path = '/home/ntomita/arradata/shared_data/temp_snapshots/vec_dict_1017_capcom.pickle'

    mapping = load_wvmap(mapping_path)
    labels_dic_path = '/home/ntomita/arra_2017/data_scripts/dict.p'
    labels_dic = pickle.load(open(labels_dic_path, 'rb'))

    topics = ['survey_tobacco',
              'survey_illegal_drugs',
              'survey_prescription_drugs',
              'survey_drinks']

    train_dataloader = MediaDataset(images_path,
                                    captions_path,
                                    comments_path,
                                    ratio,
                                    mapping,
                                    labels_dic,
                                    topics[3])
    val_dataloader = MediaDataset(images_path,
                                  join(dirname(captions_path), 'valid'),
                                  join(dirname(comments_path), 'valid'),
                                  ratio,
                                  mapping,
                                  labels_dic,
                                  topics[3])
    train_dataloader.check_consistency(ignore_imagedb=True)  # image db is empty
    val_dataloader.check_consistency(ignore_imagedb=True)
    train_dataloader.fix_imbalance()
    val_dataloader.fix_imbalance()  # should I?
    d_embed = 300
    out_size = 300
    cuda = True
    clip = 0.25
    net1 = BiLSTMModel(nlayers=2, nhid=512, in_size=d_embed,
                       out_size=out_size, need_unpack=True)
    #net2 = Linear(out_size, 1)
    net2 = Sequential(
        Dropout(p=0.5),
        Linear(out_size, 1),
        )

    #criterion = L1Loss()
    criterion = MSELoss()

    if cuda:
        net1 = net1.cuda()
        net2 = net2.cuda()
        criterion = criterion.cuda()

    optimizer = Adam(list(net1.parameters()) + list(net2.parameters()),
                     lr=2e-1, weight_decay=1e-4)

    scheduler = ReduceLROnPlateau(optimizer,
                                  mode='min',
                                  factor=0.5,
                                  verbose=1,
                                  patience=30,
                                  cooldown=10)

    num_epochs = 400  # 600
    timer = ETC(num_epochs)
    snapshot_manager = SnapshotManager(
        mode='min', every=10, ignore_first=10)
    reporter = Report(num_classes=5)

    for epoch in range(1, num_epochs+1):
        # Train
        net1.train(True)
        net2.train(True)
        epoch_loss_obj = 0.
        reporter = Report(num_classes=5)
        for i, batch in enumerate(train_dataloader, 1):
            _, caps, coms, label = batch
            h1 = net1.init_hidden(ratio[1])
            h2 = net1.init_hidden(ratio[2])
            if cuda:
                h1 = (h1[0].cuda(), h1[1].cuda())
                h2 = (h2[0].cuda(), h2[1].cuda())
            optimizer.zero_grad()
            feat_caps = net1(caps, h1)
            feat_coms = net1(coms, h2)
            feats = cat([feat_caps[-1], feat_coms[-1]], 0)
            feats = feats.view(feats.size(0), -1, feats.size(1))

            pred = net2(feats)  # [R, 1, 300] -> [R, 1, 1] where R = sum(ratio)
            label = label.repeat(sum(ratio), 1, 1)
            reporter.feed(pred, label)

            loss = criterion(pred, label)
            epoch_loss_obj += loss.data[0]
            loss.backward()
            optimizer.step()
            #clip_grad_norm(list(net1.parameters())+list(net2.parameters()), clip)
            del pred, loss
        avg_loss = epoch_loss_obj / len(train_dataloader)
        print("\nEpoch {} : Avg Loss: {:.4f}".format(epoch, avg_loss))
        print(reporter)

        # Val
        net1.train(False)
        net2.train(False)
        epoch_loss_obj = 0.
        reporter = Report(num_classes=5)
        for i, batch in enumerate(val_dataloader, 1):
            _, caps, coms, label = batch
            h1 = net1.init_hidden(ratio[1])
            h2 = net1.init_hidden(ratio[2])
            if cuda:
                h1 = (h1[0].cuda(), h1[1].cuda())
                h2 = (h2[0].cuda(), h2[1].cuda())
            feat_caps = net1(caps, h1)
            feat_coms = net1(coms, h2)
            feats = cat([feat_caps[-1], feat_coms[-1]], 0)
            feats = feats.view(feats.size(0), -1, feats.size(1))

            pred = net2(feats.detach())  # [6, 1, 300] -> [6, 1, 1]
            label = label.repeat(sum(ratio), 1, 1)
            reporter.feed(pred, label)

            loss = criterion(pred, label)
            epoch_loss_obj += loss.data[0]
            del pred, loss

        avg_loss = epoch_loss_obj / len(val_dataloader)
        print("==> Avg Loss: {:.4f}".format(avg_loss))
        print(reporter)
        scheduler.step(avg_loss, epoch)
        if snapshot_manager.need_save(avg_loss, epoch):
            torch.save(net1, join(snapshot_dir, "net1-{}.pth".format(epoch)))
        timer.tick()


if __name__ == '__main__':
    pretrain()
