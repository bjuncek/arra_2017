
captions_path = '/home/ntomita/arradata_text/tobacco/train'
comments_path = '/home/ntomita/arradata_comments/tobacco/train'
from dataset import TextDataset
from utils import load_wvmap
mapping_path = '/home/ntomita/arradata/shared_data/temp_snapshots/vec_dict_1017_capcom.pickle'
mapping = load_wvmap(mapping_path)

capdb = TextDataset(captions_path, 1, mapping, False)
len_stats = []
for k in capdb.keys:
    for tx in capdb.texts[k]:
        len_stats.append(len(tx.split()))
sstats = sorted(len_stats, reverse=True)
csvfile = open('/home/ntomita/word_freq_cap_train.csv', 'w')
for e in sstats:
    csvfile.write('{}\n'.format(e))

comdb = TextDataset(comments_path, 1, mapping, False)
len_stats = []
for k in comdb.keys:
    for tx in comdb.texts[k]:
        for s in tx.split('||'):
            len_stats.append(len(s.split()))
sstats = sorted(len_stats, reverse=True)
csvfile = open('/home/ntomita/word_freq_com_train.csv', 'w')
for e in sstats:
    csvfile.write('{}\n'.format(e))
