from PIL import Image
from os import walk
from os.path import join, dirname, basename

images = list()
target_dir = '/home/ntomita/arradata_images_original'
for dirpath, dirs, files in walk(target_dir):
    for file in files:
        if file.startswith('.'):
            continue
        path = join(dirpath, file)
        images.append(path)

for image in images:
    try:
        img = Image.open(image)
    except OSError:
        print("Broken file (OSEr): ", image)
    except AttributeError:
        print("Broken file (AtrEr): ", image)
