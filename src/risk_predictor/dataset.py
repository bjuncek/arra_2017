import torch
from torch import cat, stack, FloatTensor
from torch.autograd import Variable
from torch.utils.data import Dataset
from torch.nn.utils.rnn import pack_padded_sequence
from torchvision.transforms import Compose, RandomCrop, ToTensor, RandomHorizontalFlip, Normalize

from random import choices, shuffle, sample, random
from os import listdir, walk
from os.path import join, dirname, basename
import sys
from collections import defaultdict

from PIL import Image

from utils import generate_dicts, vectorize
from pad import pad

sys.path.append('..')
from shared.transforms import Resize
from shared.dataset_helper import compute_mean_ImageNet


def transform_ImageNet(normalize=False):
    """Based on Resnet paper
    """
    size = 256 if random() > 0.5 else 480
    transforms = [Resize(size=size),
                  RandomHorizontalFlip(),
                  RandomCrop(size=224),
                  ToTensor()]
    if normalize:
        transforms.append(Normalize(mean=[0.485, 0.456, 0.406],
                                    std=[0.229, 0.224, 0.225]))
    return Compose(transforms)


def get_label(dic, topic):
    return FloatTensor(1, 1, 1, 1).fill_(int(dic[topic]))


def get_images(path, n_samples=1):
    images_path = []
    for dirpath, dirs, files in walk(path):
        for file in files:
            images_path.append(join(path, file))
    return choices(images_path, k=n_samples)


"""
opts.datapath
opts.n_samples
opts.mapping
"""


# assumption:
# datapath structure is
# /train/survey_id/[images]
# /val/survey_id/[images]
# /test/survey_id/[images]
class ImageDataset(Dataset):
    def __init__(self, datapath, n_samples, transform, labels_dic, topic):
        self.keys = list()
        self.datapath = datapath
        self.n_samples = n_samples
        self.transform = transform
        self.labels_dic = labels_dic
        self.topic = topic
        if self.datapath is None:
            return

        for dirpath, dirs, files in walk(datapath):
            has_images = False
            for file in files:
                has_images = True
            if has_images:
                self.keys.append(basename(dirpath))
        self.keys = sorted(self.keys)

    def __getitem__(self, index):
        if self.datapath is None:
            return None, None
        key = self.keys[index]
        images = get_images(join(self.datapath, key), n_samples=self.n_samples)
        input = None

        for image in images:
            img = Image.open(image)
            if img.mode != 'RGB':
                # IRREGULAR INPUT
                img = img.convert(mode='RGB')
            ch = self.transform(img)
            ch = ch.view(1, -1, ch.size(1), ch.size(2))
            if input is None:
                input = ch
            else:
                input = cat([input, ch], 0)
        return input, None  # the dict is imcomplete, ignore

    def __len__(self):
        return len(self.keys)


class TextDataset(Dataset):
    """Dataloader for text data of arra
    Unlike other project, this datset is not supposed to work with
    torch.util.data.Dataloader. Which means it cannot produce data with batch >= 2 (as each packed sequence has different size).
    """
    def __init__(self, datapath, n_samples, mapping, cuda, max_len=None):
        self.texts, self.labels = generate_dicts(datapath)
        self.keys = sorted(list(self.texts.keys()))
        self.n_samples = n_samples
        self.mapping = mapping
        self.cuda = cuda
        self.max_len = max_len

    def get_key(self, index):
        return self.keys[index]

    def __getitem__(self, index):
        key = self.keys[index]
        sentencev = [vectorize(self.mapping, list(filter(None, sample(s.split('||'), k=1)))[0]) for s in choices(self.texts[key], k=self.n_samples)]
        seq_lens = [v.size(0) for v in sentencev]
        d = dict(zip(sentencev, seq_lens))
        # Sort sentence vectors by the length in decreasing order
        # (requirement of pack_padded_sequence)
        sentencev = sorted(sentencev, key=lambda x: d[x], reverse=True)
        sentencev = [x.view(x.size(1), x.size(0)) for x in sentencev]
        seq_lens = sorted(seq_lens, reverse=True)
        max_len = max(seq_lens)
        sentencev = [pad(v, (0, max_len-v.size(1)), 'constant', 0) for v in sentencev]
        sentencev = stack(sentencev, 0).view(max_len, len(seq_lens), -1)
        label = Variable(FloatTensor(self.n_samples, 1).fill_(int(self.labels[key][0])))

        if self.cuda:
            sentencev = sentencev.cuda()
            label = label.cuda()
        return pack_padded_sequence(sentencev, seq_lens), label

    def __len__(self):
        return len(self.keys)


class MediaDataset(Dataset):
    """A multimedia dataset that holds different dataloader inside and
    provide a set of media data for a user at a time
    For Arra project, this will have three dataloaders:
        images
        captions
        comments
    """
    def __init__(self, images_path, captions_path, comments_path, nums_samples,
                 mapping, labels_dic, topic, binarize=False):
        self.labels_dic = labels_dic  # use this and topic for fix_imbalance
        self.topic = topic
        self.binarize = binarize
        self.image_dataset = ImageDataset(images_path,
                                          nums_samples[0],
                                          transform_ImageNet(normalize=True),
                                          labels_dic,
                                          topic)
        self.caption_dataset = TextDataset(captions_path,
                                           nums_samples[1],
                                           mapping, True)
        self.comment_dataset = TextDataset(comments_path,
                                           nums_samples[2],
                                           mapping, True)

    def __getitem__(self, index):
        images, _ = self.image_dataset[index]
        caps, _ = self.caption_dataset[index]
        coms, _ = self.comment_dataset[index]
        key = self.caption_dataset.get_key(index)
        # THIS SHOULD BE RE-ORGANIZED (CURRENTLY DIRTY FIX)
        if self.binarize:
            label = 1 if int(self.labels_dic[key][self.topic]) > 0 else 0
            label = Variable(FloatTensor(1).fill_(label))
        else:
            label = Variable(FloatTensor(1).fill_(int(self.labels_dic[key][self.topic])))
        label = label.cuda()
        return images, caps, coms, label

    def __len__(self):
        return len(self.caption_dataset)

    def check_consistency(self, ignore_imagedb=False):
        if not ignore_imagedb:
            print("key size of image db: {}".format(len(self.image_dataset)))
        print("key size of caption db: {}".format(len(self.caption_dataset)))
        print("key size of comment db: {}".format(len(self.comment_dataset)))
        if not ignore_imagedb:
            intersection = list(set(self.image_dataset.keys) & set(self.caption_dataset.keys))
        else:
            intersection = list(set(self.caption_dataset.keys))
        intersection = list(set(intersection) & set(self.comment_dataset.keys))
        print("key size of the intersection: {}".format(len(intersection)))

        if not ignore_imagedb:
            self.image_dataset.keys = intersection
        self.caption_dataset.keys = intersection
        self.comment_dataset.keys = intersection

    def fix_imbalance(self):
        """ Fix the possible class imbalance problem in dataset
            by oversampling smaller classes
            This function should be called after checking consistency
            of the data (esp. uid) across image, caption, and comment
            dataset.
        """
        bins = defaultdict(list)
        for key in self.caption_dataset.keys:
            try:
                key_class = self.labels_dic[key][self.topic]
                if self.binarize and key_class > 0:
                    key_class = 1
                bins[key_class].append(key)
            except:
                continue
        largest_size = max([len(bins[k]) for k in bins.keys()])
        print(largest_size)
        for k in bins.keys():
            print(len(bins[k]))
        new_db_keys = []
        for k in bins.keys():
            new_db_keys += bins[k]  # guarantee all the elements appear at least once
            n_extra_samples = largest_size - len(bins[k])
            new_db_keys += choices(bins[k], k=n_extra_samples)
        shuffle(new_db_keys)
        print(len(new_db_keys))
        self.image_dataset.keys = new_db_keys
        self.caption_dataset.keys = new_db_keys
        self.comment_dataset.keys = new_db_keys




