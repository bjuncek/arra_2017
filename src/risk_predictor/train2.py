import torch
from torch import nn, cat, LongTensor
from torch.nn import functional as F
from torch.nn import L1Loss, MSELoss, Linear, Dropout, Sequential, CrossEntropyLoss, Sigmoid
from torch.nn.utils import clip_grad_norm  # cliping not needed if use weight decay
from torch.utils.data import DataLoader
from torch.autograd import Variable
from torch.optim import Adam
from torchtext import datasets, data
from torchtext.data import Field

import argparse
from os import makedirs
from os.path import join, isfile, exists, dirname
import pickle

from utils import load_wvmap, vectorize, generate_dicts, weight_init
from dataset import TextDataset, MediaDataset
from models import create_resnet, LSTMModel
from report import Report

import sys
sys.path.append('..')
from shared.utils import set_all_seeds, SnapshotManager, ETC
from shared.lr_scheduler import ReduceLROnPlateau

TOPICS = ['breakup',
          'creativity',
          'death_love',
          'drinks',
          'food',
          'happy',
          'illegal_drugs',
          'major_friendship',
          'narcissist',
          'other_loss',
          'prescription_drugs',
          'tobacco']  # 11 topics


""" Training by different strategy:
        Setting up lstms for each data source, and aggregated feature
        vectors for each source are then fed to dense prediction layer.
"""

def train(argv=sys.argv[1:]):
    print(argv)
    parser = argparse.ArgumentParser()
    parser.add_argument('--snapshot_dir', '-s',
                        type=str,
                        help='path to save snapshots of models')
    parser.add_argument('--topic', '-t',
                        type=int,
                        help='index of target topic')
    parser.add_argument('--lr', '-l',
                        type=float,
                        default=1e-2,
                        help='initial learning rate')
    parser.add_argument('--binarize', '-b',
                        action='store_true',
                        default=False)
    parser.add_argument('--on_server',
                        action='store_true',
                        default=False)
    parser.add_argument('--nhid_factor',
                        type=int,
                        default=1)
    parser.add_argument('--epochs',
                        type=int,
                        default=400)
    args = parser.parse_args(argv)

    """
    E2E Training script for image, captions and comments
        by aggregating features with another LSTM
    """
    on_server = args.on_server
    topic = TOPICS[args.topic]
    print("Selected topic: {}".format(topic))
    snapshot_dir = args.snapshot_dir
    num_classes = 2 if args.binarize else 5

    images_path = '/home/ntomita/master_dataset_v2'
    captions_path = join('/home/ntomita/arradata_rev1_captions', topic)
    comments_path = join('/home/ntomita/arradata_rev1_comments', topic)
    mapping_path = '/home/ntomita/arradata/shared_data/temp_snapshots/vec_dict_1017_capcom.pickle'
    labels_dic_path = '/home/ntomita/arra_2017/data_scripts/userdata.pickle'

    if on_server:
        images_path = '/pool1/ntomita/master_dataset_v2'
        captions_path = join('/pool1/ntomita/arradata_rev1_captions', topic)
        comments_path = join('/pool1/ntomita/arradata_rev1_comments', topic)
        mapping_path = '/pool1/ntomita/shared_data/temp_snapshots/vec_dict_1017_capcom.pickle'
        labels_dic_path = '/pool1/ntomita/shared_data/userdata.pickle'
    ratio = [20, 20, 40]

    makedirs(snapshot_dir, exist_ok=True)

    mapping = load_wvmap(mapping_path)
    labels_dic = pickle.load(open(labels_dic_path, 'rb'))

    topic = 'survey_' + topic

    train_dataloader = MediaDataset(join(images_path, 'train'),
                                    join(captions_path, 'train'),
                                    join(comments_path, 'train'),
                                    ratio,
                                    mapping,
                                    labels_dic,
                                    topic=topic,
                                    binarize=args.binarize)
    val_dataloader = MediaDataset(join(images_path, 'val'),
                                  join(captions_path, 'valid'),
                                  join(comments_path, 'valid'),
                                  ratio,
                                  mapping,
                                  labels_dic,
                                  topic=topic,
                                  binarize=args.binarize)

    train_dataloader.check_consistency()
    val_dataloader.check_consistency()
    train_dataloader.fix_imbalance()
    val_dataloader.fix_imbalance()

    d_embed = 300
    out_size = 300
    cuda = True

    """ net_vis: resnet (image feature extraction)
        net_seq1: lstm (text feature extraction )
        net_seq2: lstm (aggregation of image and text features)

        net_vis_agg: lstm that aggregates features extracted from net_vis
        net_seq1_agg: lstm that aggregates features extracted from net_seq1
        net_seq2_agg: lstm that aggregates features extracted from net_seq1
    """
    net_vis = create_resnet()

    fnhid = args.nhid_factor
    net_seq1 = LSTMModel(nlayers=1, nhid=256*fnhid, in_size=d_embed,
                         out_size=out_size, need_unpack=True)
    net_seq2 = LSTMModel(nlayers=1, nhid=256*fnhid, in_size=d_embed,
                         out_size=out_size, need_unpack=True)

    net_vis_agg = LSTMModel(nlayers=1, nhid=128*fnhid, in_size=out_size,
                            out_size=out_size//3, need_unpack=False)
    net_seq1_agg = LSTMModel(nlayers=1, nhid=128*fnhid, in_size=out_size,
                             out_size=out_size//3, need_unpack=False)
    net_seq2_agg = LSTMModel(nlayers=1, nhid=128*fnhid, in_size=out_size,
                             out_size=out_size//3, need_unpack=False)
    net_agg = Sequential(Linear(out_size, num_classes))  # need dropout?

    for n in [net_vis, net_seq1, net_seq2, net_vis_agg, net_seq1_agg, net_seq2_agg, net_agg]:
        n.apply(weight_init)

    criterion = CrossEntropyLoss()

    if cuda:
        net_vis = net_vis.cuda()
        net_seq1 = net_seq1.cuda()
        net_seq2 = net_seq2.cuda()
        net_vis_agg = net_vis_agg.cuda()
        net_seq1_agg = net_seq1_agg.cuda()
        net_seq2_agg = net_seq2_agg.cuda()
        net_agg = net_agg.cuda()
        criterion = criterion.cuda()

    params = list(net_vis.parameters()) +\
        list(net_seq1.parameters()) +\
        list(net_seq2.parameters()) +\
        list(net_vis_agg.parameters()) +\
        list(net_seq1_agg.parameters()) +\
        list(net_seq2_agg.parameters()) +\
        list(net_agg.parameters())

    optimizer = Adam(params, lr=args.lr, weight_decay=1e-4)
    scheduler = ReduceLROnPlateau(optimizer,
                                  mode='min',
                                  factor=0.5,
                                  verbose=1,
                                  patience=20,
                                  cooldown=10)

    num_epochs = args.epochs
    timer = ETC(num_epochs)
    snapshot_manager = SnapshotManager(
        mode='min', every=10, ignore_first=10)

    def train_epoch(epoch):
        net_vis.train(True)
        net_seq1.train(True)
        net_seq2.train(True)
        net_vis_agg.train(True)
        net_seq1_agg.train(True)
        net_seq2_agg.train(True)
        net_agg.train(True)

        epoch_loss_obj = 0.
        reporter = Report(num_classes=num_classes)
        for i, batch in enumerate(train_dataloader, 1):
            images, caps, coms, label = batch
            label = label.type(LongTensor)
            images = Variable(images)
            hs1 = net_seq1.init_hidden(ratio[1])
            hs2 = net_seq2.init_hidden(ratio[2])
            hva = net_vis_agg.init_hidden(batch_size=1)
            hs1a = net_seq1_agg.init_hidden(batch_size=1)
            hs2a = net_seq2_agg.init_hidden(batch_size=1)

            if cuda:
                images = images.cuda()
                label = label.cuda()
                hs1 = (hs1[0].cuda(), hs1[1].cuda())
                hs2 = (hs2[0].cuda(), hs2[1].cuda())
                hva = (hva[0].cuda(), hva[1].cuda())
                hs1a = (hs1a[0].cuda(), hs1a[1].cuda())
                hs2a = (hs2a[0].cuda(), hs2a[1].cuda())

            optimizer.zero_grad()

            feat_imgs = net_vis(images)  # T,Outsize
            feat_caps = net_seq1(caps, hs1)[-1]  # T,Outsize
            feat_coms = net_seq2(coms, hs2)[-1]  # T,Outsize

            aggfeat_imgs = net_vis_agg(feat_imgs.view(feat_imgs.size(0),
                                                      -1,
                                                      feat_imgs.size(1)),
                                       hva)[-1]
            aggfeat_caps = net_vis_agg(feat_caps.view(feat_caps.size(0),
                                                      -1,
                                                      feat_caps.size(1)),
                                       hs1a)[-1]
            aggfeat_coms = net_vis_agg(feat_coms.view(feat_coms.size(0),
                                                      -1,
                                                      feat_coms.size(1)),
                                       hs2a)[-1]
            # T,Outsize

            feats = cat([aggfeat_imgs, aggfeat_caps, aggfeat_coms], 0)
            feats = feats.view(1, -1)
            pred = net_agg(feats)

            loss = criterion(pred, label)
            reporter.feed_softmax(pred, label)

            epoch_loss_obj += loss.data[0]
            loss.backward()
            optimizer.step()

            del images, feat_imgs, feat_caps, feat_coms
            del aggfeat_imgs, aggfeat_caps, aggfeat_coms
            del hs1, hs2, hva, hs1a, hs2a
            del pred, loss
        avg_loss = epoch_loss_obj / len(train_dataloader)
        print("\nEpoch {} : Avg Loss: {:.4f}".format(epoch, avg_loss))
        print(reporter)
        print(reporter.show_stats())

    def validate(epoch):
        net_vis.train(False)
        net_seq1.train(False)
        net_seq2.train(False)
        net_vis_agg.train(False)
        net_seq1_agg.train(False)
        net_seq2_agg.train(False)
        net_agg.train(False)

        epoch_loss_obj = 0.
        reporter = Report(num_classes=num_classes)
        for i, batch in enumerate(val_dataloader, 1):
            images, caps, coms, label = batch
            label = label.type(LongTensor)
            images = Variable(images)
            hs1 = net_seq1.init_hidden(ratio[1])
            hs2 = net_seq2.init_hidden(ratio[2])
            hva = net_vis_agg.init_hidden(batch_size=1)
            hs1a = net_seq1_agg.init_hidden(batch_size=1)
            hs2a = net_seq2_agg.init_hidden(batch_size=1)

            if cuda:
                images = images.cuda()
                label = label.cuda()
                hs1 = (hs1[0].cuda(), hs1[1].cuda())
                hs2 = (hs2[0].cuda(), hs2[1].cuda())
                hva = (hva[0].cuda(), hva[1].cuda())
                hs1a = (hs1a[0].cuda(), hs1a[1].cuda())
                hs2a = (hs2a[0].cuda(), hs2a[1].cuda())

            optimizer.zero_grad()

            feat_imgs = net_vis(images)  # T,Outsize
            feat_caps = net_seq1(caps, hs1)[-1]  # T,Outsize
            feat_coms = net_seq2(coms, hs2)[-1]  # T,Outsize

            aggfeat_imgs = net_vis_agg(feat_imgs.view(feat_imgs.size(0),
                                                      -1,
                                                      feat_imgs.size(1)),
                                       hva)[-1]
            aggfeat_caps = net_vis_agg(feat_caps.view(feat_caps.size(0),
                                                      -1,
                                                      feat_caps.size(1)),
                                       hs1a)[-1]
            aggfeat_coms = net_vis_agg(feat_coms.view(feat_coms.size(0),
                                                      -1,
                                                      feat_coms.size(1)),
                                       hs2a)[-1]
            # T,Outsize

            feats = cat([aggfeat_imgs, aggfeat_caps, aggfeat_coms], 0)
            feats = feats.view(1, -1)
            pred = net_agg(feats)

            loss = criterion(pred, label)
            reporter.feed_softmax(pred, label)

            epoch_loss_obj += loss.data[0]
            del images, feat_imgs, feat_caps, feat_coms
            del aggfeat_imgs, aggfeat_caps, aggfeat_coms
            del hs1, hs2, hva, hs1a, hs2a
            del pred, loss
        avg_loss = epoch_loss_obj / len(val_dataloader)
        print("==> Avg Loss: {:.4f}".format(avg_loss))
        print(reporter)
        scheduler.step(avg_loss, epoch)
        if snapshot_manager.need_save(avg_loss, epoch):
            torch.save(net_vis, join(snapshot_dir, "net_vis-{}.pth".format(epoch)))
            torch.save(net_seq1, join(snapshot_dir, "net_seq1-{}.pth".format(epoch)))
            torch.save(net_seq2, join(snapshot_dir, "net_seq2-{}.pth".format(epoch)))
            torch.save(net_vis_agg, join(snapshot_dir, "net_vis_agg-{}.pth".format(epoch)))
            torch.save(net_seq1_agg, join(snapshot_dir, "net_seq1_agg-{}.pth".format(epoch)))
            torch.save(net_seq2_agg, join(snapshot_dir, "net_seq2_agg-{}.pth".format(epoch)))
            torch.save(net_agg, join(snapshot_dir, "net_agg-{}.pth".format(epoch)))

    for epoch in range(1, num_epochs+1):
        sys.stdout.flush()
        train_epoch(epoch)
        validate(epoch)
        timer.tick()


if __name__ == '__main__':
    train()
