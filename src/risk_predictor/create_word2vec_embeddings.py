# Copyright 2015 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

# from __future__ import absolute_import
# from __future__ import print_function
import tensorflow as tf
import tensorflow.python.platform
import collections
import math
import numpy as np
import os
from os.path import isdir
import random
import zipfile
import pickle
import sys
import argparse
from collections import Counter
from time import ctime, time


class ETC:
    """
    A class to estimate the time of completion for current training
    tick() at the end of training each iteration including on validation set
    """
    def __init__(self, n_epoch, make_pred_at=2):
        assert n_epoch > 0, make_pred_at > 0
        self.n_epoch = n_epoch
        self.make_pred_at = make_pred_at
        self.current_t = 0
        self.prev_time = time()
        self.sum_deltatime = 0.0

    def tick(self):
        if self.current_t > self.make_pred_at:
            return
        self.sum_deltatime += time() - self.prev_time
        self.prev_time = time()
        self.current_t += 1
        if self.current_t == self.make_pred_at:
            avg_time_taken = self.sum_deltatime / self.current_t
            etc = avg_time_taken*(self.n_epoch-self.current_t) + self.prev_time
            print("INFO: Estimated Time of Completion (ETC): " + ctime(int(etc)))


def main(argv=sys.argv[1:]):
    print(argv)
    parser = argparse.ArgumentParser()
    parser.add_argument('--zipfile', '-z',
                        type=str,
                        help='path of a zip file containing all texts')
    parser.add_argument('--batchsize', '-b',
                        type=int,
                        default=128,
                        help='the size of mini batch (default: 128)')
    parser.add_argument('--lr', '-l',
                        type=float,
                        default=0.5,
                        help='initial learning rate (default: 0.5)')
    parser.add_argument('--niters', '-n',
                        type=int,
                        default=2000000,
                        help='the number of iterations for training (default: 2000000)')
    parser.add_argument('--wvdim', '-d',
                        type=int,
                        default=128,
                        help='the dimension of embeddings (default: 128)')
    parser.add_argument('--skipwindow', '-w',
                        type=int,
                        default=2,
                        help='the size of context window on one side (default: 2)')
    parser.add_argument('--gpus',
                        type=int,
                        default=1,
                        help='the number of gpus to be used (set 0 for cpu) (currently multi-gpus is not supported, default: 1)')
    parser.add_argument('--validation', '-v',
                        action='store_true',
                        help='show several examples of embeddings')
    parser.add_argument('--silent',
                        action='store_true',
                        help='supress output')

    args = parser.parse_args(argv)

    filename = args.zipfile
    devices = '/cpu:0' if args.gpus == 0 else ["/gpu:{}".format(i) for i in range(args.gpus)]
    devices = devices[0] if (isinstance(devices, list) and len(devices)==1) else devices

    # Read the data into a string.
    # CAUSE OF MEMORY EXPLOSION
    def read_data(filename):
        f = zipfile.ZipFile(filename)
        strlist = []
        for name in f.namelist():
            if not isdir(name):
                strlist += f.read(name).split()
        f.close()
        return strlist

    # Generator version of read_data function
    # It reads and returns a list of words, line by line, file by file
    # that prevents load all the data into memory at once
    def generate_data(filename):
        with zipfile.ZipFile(filename) as z:
            for name in z.namelist():
                if not isdir(name):
                    with z.open(name) as f:
                        for line in f:
                            yield line.split()

    # Step 2: Build the dictionary and replace rare words with UNK token.
    vocabulary_size = 500000

    def build_dataset(filename):
        count = [['UNK', -1]]
        c = Counter()
        for words in generate_data(filename):
            c + Counter(words)
        count.extend(c.most_common(vocabulary_size-1))

        dictionary = dict()
        for word, _ in count:
            dictionary[word] = len(dictionary)

        data = list()
        unk_count = 0

        for words in generate_data(filename):
            for word in words:
                if word in dictionary:
                    index = dictionary[word]
                else:
                    index = 0  # dictionary['UNK']
                    unk_count += 1
                data.append(index)
        count[0][1] = unk_count

        reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
        return data, count, dictionary, reverse_dictionary

    data, count, dictionary, reverse_dictionary = build_dataset(filename)

    if args.silent is None:
        print('Most common words (+UNK)', count[:5])
        print('Sample data', data[:10])

    # Step 3: Function to generate a training batch for the skip-gram model.
    class DataLoader():
        def __init__(self, data):
            self.data = data
            self.data_index = 0

        def generate_batch(self, batch_size, num_skips, skip_window):
            assert batch_size % num_skips == 0
            assert num_skips <= 2 * skip_window
            batch = np.ndarray(shape=(batch_size), dtype=np.int32)
            labels = np.ndarray(shape=(batch_size, 1), dtype=np.int32)
            span = 2 * skip_window + 1  # [ skip_window target skip_window ]
            buffer = collections.deque(maxlen=span)

            for _ in range(span):
                buffer.append(self.data[self.data_index])
                self.data_index = (self.data_index + 1) % len(self.data)

            for i in range(batch_size // num_skips):
                target = skip_window  # target label at the center of the buffer
                targets_to_avoid = [skip_window, ]
                for j in range(num_skips):
                    while target in targets_to_avoid:
                        target = random.randint(0, span - 1)
                    targets_to_avoid.append(target)
                    batch[i * num_skips + j] = buffer[skip_window]
                    labels[i * num_skips + j, 0] = buffer[target]
                buffer.append(self.data[self.data_index])
                self.data_index = (self.data_index + 1) % len(self.data)
            return batch, labels

    dataloader = DataLoader(data)
    batch, labels = dataloader.generate_batch(batch_size=8, num_skips=2, skip_window=1)

    if args.silent is None:
        for i in range(8):
            print(batch[i], '->', labels[i, 0])
            print(reverse_dictionary[batch[i]], '->', reverse_dictionary[labels[i, 0]])

    # Step 4: Build and train a skip-gram model.
    batch_size = args.batchsize
    embedding_size = args.wvdim   # 128  # Dimension of the embedding vector.
    skip_window = args.skipwindow  # 1       # How many words to consider left and right.
    num_skips = 2         # How many times to reuse an input to generate a label.

    # We pick a random validation set to sample nearest neighbors. Here we limit the
    # validation samples to the words that have a low numeric ID, which by
    # construction are also the most frequent.

    valid_size = 16     # Random set of words to evaluate similarity on.
    valid_window = 100  # Only pick dev samples in the head of the distribution.

    valid_examples = np.array(random.sample(list(np.arange(valid_window)), valid_size))
    num_sampled = 64    # Number of negative examples to sample.

    graph = tf.Graph()
    with graph.as_default():

        # Input data.
        train_inputs = tf.placeholder(tf.int32, shape=[batch_size])
        train_labels = tf.placeholder(tf.int32, shape=[batch_size, 1])
        valid_dataset = tf.constant(valid_examples, dtype=tf.int32)

        # Ops and variables pinned to the CPU because of missing GPU implementation
        with tf.device(devices):
            # Look up embeddings for inputs.
            embeddings = tf.Variable(
                    tf.random_uniform([vocabulary_size, embedding_size], -1.0, 1.0))
            embed = tf.nn.embedding_lookup(embeddings, train_inputs)

            # Construct the variables for the NCE loss
            nce_weights = tf.Variable(
                    tf.truncated_normal([vocabulary_size, embedding_size],
                                                            stddev=1.0 / math.sqrt(embedding_size)))
            nce_biases = tf.Variable(tf.zeros([vocabulary_size]))

        # Compute the average NCE loss for the batch.
        # tf.nce_loss automatically draws a new sample of the negative labels each
        # time we evaluate the loss.
        loss = tf.reduce_mean(
                tf.nn.nce_loss(weights=nce_weights, biases=nce_biases,inputs=embed,labels=train_labels,
                                             num_sampled=num_sampled, num_classes=vocabulary_size))

        # Construct the SGD optimizer using a learning rate of 1.0.
        global_step = tf.Variable(0, trainable=False)
        starter_lr = args.lr
        lr = tf.train.exponential_decay(starter_lr,
                                        global_step,
                                        args.niters / 100,
                                        0.96,
                                        staircase=True)
        optimizer = tf.train.GradientDescentOptimizer(lr).minimize(loss, global_step=global_step)

        # Compute the cosine similarity between minibatch examples and all embeddings.
        norm = tf.sqrt(tf.reduce_sum(tf.square(embeddings), 1, keep_dims=True))
        normalized_embeddings = embeddings / norm
        valid_embeddings = tf.nn.embedding_lookup(
                normalized_embeddings, valid_dataset)
        similarity = tf.matmul(
                valid_embeddings, normalized_embeddings, transpose_b=True)

    # Step 5: Begin training.
    num_steps = args.niters
    dataloader = DataLoader(data)
    with tf.Session(graph=graph) as session:
        # We must initialize all variables before we use them.
        tf.global_variables_initializer().run()
        if args.silent is None:
            print("Initialized")
        average_loss = 0.

        observations = num_steps / 1000  # 1000 times of outputs
        timer = ETC(observations, make_pred_at=10)
        for step in range(1, num_steps+1):
            batch_inputs, batch_labels = dataloader.generate_batch(
                    batch_size, num_skips, skip_window)
            feed_dict = {train_inputs : batch_inputs, train_labels : batch_labels}

            # We perform one update step by evaluating the optimizer op (including it
            # in the list of returned values for session.run()

            _, loss_val = session.run([optimizer, loss], feed_dict=feed_dict)
            average_loss += loss_val

            if step % observations == 0:
                if step > 0:
                    average_loss /= observations
                # The average loss is an estimate of the loss over the last 2000 batches.
                print("Average loss at step ", step, ": ", average_loss)

                average_loss = 0
                timer.tick()

            if args.validation is True and step % (observations*10) == 0:
                sim = similarity.eval()
                for i in range(valid_size):
                    valid_word = reverse_dictionary[valid_examples[i]]
                    top_k = 8  # number of nearest neighbors
                    nearest = (-sim[i, :]).argsort()[1:top_k+1]
                    # log_str = "Nearest to %s:" % valid_word
                    log_str = "Nearest to {}:".format(valid_word)
                    for k in range(top_k):
                        close_word = reverse_dictionary[nearest[k]]
                        log_str = "%s %s," % (log_str, close_word)
                    print(log_str)
        final_embeddings = normalized_embeddings.eval()

    # Step 6: Visualize the embeddings.
    def plot_with_labels(low_dim_embs, labels, filename='tsne.png'):
        assert low_dim_embs.shape[0] >= len(labels), "More labels than embeddings"
        plt.figure(figsize=(18, 18))  #in inches
        for i, label in enumerate(labels):
            x, y = low_dim_embs[i,:]
            plt.scatter(x, y)
            plt.annotate(label,
                                     xy=(x, y),
                                     xytext=(5, 2),
                                     textcoords='offset points',
                                     ha='right',
                                     va='bottom')
        plt.savefig(filename)


    #try:
    #  from sklearn.manifold import TSNE
    #  import matplotlib.pyplot as plt
    #  tsne = TSNE(perplexity=30, n_components=2, init='pca', n_iter=5000)
    #  plot_only = 500
    #  low_dim_embs = tsne.fit_transform(final_embeddings[:plot_only,:])
    #  labels = [reverse_dictionary[i] for i in xrange(plot_only)]
    #  plot_with_labels(low_dim_embs, labels)
    #except ImportError:
    #  print("Please install sklearn and matplotlib to visualize embeddings.")


    ## Write the embeddings
    print(len(reverse_dictionary), len(final_embeddings))
    c = dict(zip(reverse_dictionary.values(), final_embeddings))
    pickle.dump(c, open('vecDict.pickle', 'wb+'))


if __name__ == '__main__':
    main()
