import glob
import os
import pickle
import argparse
from utils import load_wvmap

def main(argv=sys.argv[1:]):
    if sys.version_info[0] < 3:
        raise "Need Python3.x"
    parser = argparse.ArgumentParser()
    parser.add_argument('--dictfile', '-d',
                        type=str,
                        help='path of a pickled dictionary file containing word embeddings')
    args = parser.parse_args(argv)
    dictionary = load_wvdict(args.dictfile)

    cases = ['negative', 'positive']
    for case in cases:
        os.chdir("/Users/Saeed/Dropbox/Project/Summarization/Classes/finding/corpus/test/" + case )
        for fileName in glob.glob("*.txt"):
            file = open(fileName, 'r')
            words=file.read().split()
            vector = [0] * len(w2v['UNK'])
            for word in  words:
                if word in w2v:
                    v = w2v[word]
                else:
                    v = w2v['UNK']
                vector = map(sum, zip(vector,v))
            output_file = open("../../../word2vec/" + fileName , "w")
            c = 1
            for i in vector:
                output_file.write('%s:' %c)
                output_file.write('%s ' %i)
                c = c+1
            output_file.close()


if __name__ == '__main__':
    main()