from torch.autograd import Function, Variable

# Turn the parameter pad in pytorch into paddings in onnx order.
def prepare_paddings(input, pad):
    dim = len(input.type().sizes())
    # The order of paddings is dim_0_begin, dim_0_end, dim_1_begin, ... , dim_n_end.
    # n is the dimension of input.
    assert len(pad) <= dim * 2
    paddings = []
    # pad is guaranteed to have even elements.
    for i, j in zip(pad[0::2], pad[1::2]):
        paddings = [i, j] + paddings
    while len(paddings) < 2 * dim:
        paddings = [0, 0] + paddings
    assert len(paddings) == dim * 2
    return paddings


class ConstantPadNd(Function):

    @staticmethod
    def symbolic(g, input, pad, value=0):
        paddings = prepare_paddings(input, pad)
        return g.appendNode(g.create("Pad", [input]).is_("paddings", paddings)
                             .s_("mode", "constant").f_("value", value))

    @staticmethod
    def forward(ctx, input, pad, value=0):
        ctx.pad = pad
        ctx.value = value
        ctx.input_size = input.size()
        ctx.l_inp = len(input.size())
        ctx.pad_tup = tuple([(a, b) for a, b in zip(pad[:-1:2], pad[1::2])][::-1])
        ctx.l_pad = len(ctx.pad_tup)
        ctx.l_diff = ctx.l_inp - ctx.l_pad
        assert ctx.l_inp >= ctx.l_pad

        new_dim = tuple([sum((d,) + ctx.pad_tup[i]) for i, d in enumerate(input.size()[-ctx.l_pad:])])
        assert all([d > 0 for d in new_dim]), 'input is too small'

        # crop input if necessary
        output = input.new(input.size()[:(ctx.l_diff)] + new_dim).fill_(ctx.value)
        c_input = input

        for i, p in zip(range(ctx.l_inp)[-ctx.l_pad:], ctx.pad_tup):
            if p[0] < 0:
                c_input = c_input.narrow(i, -p[0], c_input.size(i) + p[0])
            if p[1] < 0:
                c_input = c_input.narrow(i, 0, c_input.size(i) + p[1])

        # crop output if necessary
        c_output = output
        for i, p in zip(range(ctx.l_inp)[-ctx.l_pad:], ctx.pad_tup):
            if p[0] > 0:
                c_output = c_output.narrow(i, p[0], c_output.size(i) - p[0])
            if p[1] > 0:
                c_output = c_output.narrow(i, 0, c_output.size(i) - p[1])
        c_output.copy_(c_input)
        return output

    @staticmethod
    def backward(ctx, grad_output):
        grad_input = Variable(grad_output.data.new(ctx.input_size).zero_())
        grad_input_slices = [slice(0, x,) for x in ctx.input_size]

        def narrow_slice(dim, start, length):
            grad_input_slices[dim] = (slice(grad_input_slices[dim].start + start,
                                            grad_input_slices[dim].start + start + length))

        def slice_length(dim):
            return grad_input_slices[dim].stop - grad_input_slices[dim].start

        #  crop grad_input if necessary
        for i, p in zip(range(ctx.l_inp)[-ctx.l_pad:], ctx.pad_tup):
            if p[0] < 0:
                narrow_slice(i, -p[0], slice_length(i) + p[0])
            if p[1] < 0:
                narrow_slice(i, 0, slice_length(i) + p[1])

        # crop grad_output if necessary
        cg_output = grad_output
        for i_s, p in zip(range(ctx.l_inp)[-ctx.l_pad:], ctx.pad_tup):
            if p[0] > 0:
                cg_output = cg_output.narrow(i_s, p[0], cg_output.size(i_s) - p[0])
            if p[1] > 0:
                cg_output = cg_output.narrow(i_s, 0, cg_output.size(i_s) - p[1])
        gis = tuple(grad_input_slices)
        grad_input[gis] = cg_output

        return grad_input, None, None

def pad(input, pad, mode='constant', value=0):
    """Pads tensor.

    Nd constant padding:  The number of dimensions to pad is
        len(padding) // 2 and the dimensions that gets padded begins with the
        last dimension and moves forward.  See below for examples.

    1D, 2D and 3D "reflect"/"replicate" padding:
        1D: 3D input with padding in form (pad_l, pad_r)
        2D: 4D input tensor pad should be in form
        (pad_l, pad_r, pad_t, pad_b ).
        3D: 5D pad (pleft, pright, ptop, pbottom, pfront, pback). No "reflect"
        implementation

    Args:
        input (Variable): Nd tensor
        pad (tuple): m-elem tuple, where m // 2 > input dimensions and m % 2 == 0
        mode: 'constant', 'reflect' or 'replicate'. Default: 'constant'
        value: fill value for 'constant' padding. Default: 0

    Examples::

        >>> t4d = torch.Tensor(3, 3, 4, 2)
        >>> p1d = (1, 1) # pad last dim by 1 on each side
        >>> out = F.pad(t4d, p1d, "constant", 0)
        >>> print(out.data.size())
        torch.Size([3, 3, 4, 4])
        >>> p2d = (1, 1, 2, 2) # pad last dim by (1, 1) and 2nd to last by (2, 2)
        >>> out = F.pad(t4d, p2d, "constant", 0)
        >>> print(out.data.size())
        torch.Size([3, 3, 8, 4])
        >>> t4d = torch.Tensor(3, 3, 4, 2)
        >>> p3d = (0, 1, 2, 1, 3, 3) # pad by (0, 1), (2, 1), and (3, 3)
        >>> out = F.pad(t4d, p3d, "constant", 0)
        >>> print(out.data.size())
        torch.Size([3, 9, 7, 3])
    """
    assert len(pad) % 2 == 0, 'padding length must be divisible by 2'
    assert len(pad) // 2 <= len(input.size()), 'padding length too large'
    if mode == 'constant':
        return ConstantPadNd.apply(input, pad, value)
    elif input.dim() == 3:
        assert len(pad) == 2, '3D tensors expect 2 values for padding'
        if mode == 'reflect':
            return _functions.thnn.ReflectionPad1d.apply(input, *pad)
        elif mode == 'replicate':
            return _functions.thnn.ReplicationPad1d.apply(input, *pad)
    elif input.dim() == 4:
        assert len(pad) == 4, '4D tensors expect 4 values for padding'
        if mode == 'reflect':
            return _functions.thnn.ReflectionPad2d.apply(input, *pad)
        elif mode == 'replicate':
            return _functions.thnn.ReplicationPad2d.apply(input, *pad)
    elif input.dim() == 5:
        assert len(pad) == 6, '5D tensors expect 6 values for padding'
        if mode == 'reflect':
            raise NotImplementedError
        elif mode == 'replicate':
            return _functions.thnn.ReplicationPad3d.apply(input, *pad)
    else:
        raise NotImplementedError("Only 3D, 4D, 5D padding with non-constant padding are supported for now")