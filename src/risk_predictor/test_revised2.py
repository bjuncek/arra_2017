import torch
from torch import nn, cat, LongTensor
from torch.nn import CrossEntropyLoss
from torch.utils.data import DataLoader
from torch.autograd import Variable

import argparse
from os import makedirs
from os.path import join, isfile, exists, dirname
import pickle
import sys
from collections import defaultdict
from statistics import mean
from numpy import zeros, matrix, array

from utils import load_wvmap
from dataset_revised import MediaDataset
from report import Report
from roc_figure import save_roc_curve

TOPICS = ['breakup',
          'creativity',
          'death_love',
          'drinks',
          'food',
          'happy',
          'illegal_drugs',
          'major_friendship',
          'narcissist',
          'other_loss',
          'prescription_drugs',
          'tobacco']  # 11 topics


def compute_roc(preds, labels):
    """ In:
        preds: list of float
        labels: list of bools

        Out:
        tprs: list of float
        fprs: list of float

    """
    eps = sys.float_info.epsilon
    pl_sorted = [(x, y) for x, y in sorted(zip(preds, labels), key=lambda pl: pl[0])]
    thresholds = [0.0,] + [x for x, y in pl_sorted] + [1.0,]
    tprs = list()
    fprs = list()

    for threshold in thresholds:
        results = [(p >= threshold, l) for p, l in pl_sorted]
        tps = sum([p and l for p, l in results])
        fns = sum([(not p) and l for p, l in results])
        fps = sum([p and (not l) for p, l in results])
        tns = sum([not (p or l) for p, l in results])
        tpr = tps / (tps + fns + eps)
        fpr = fps / (fps + tns + eps)
        tprs.append(tpr)
        fprs.append(fpr)
    return tprs, fprs


def test(argv=sys.argv[1:]):
    print(argv)
    parser = argparse.ArgumentParser()
    parser.add_argument('--model0',
                        type=str,
                        help='path to model 0')
    parser.add_argument('--model1',
                        type=str,
                        help='path to model 1')
    parser.add_argument('--model2',
                        type=str,
                        help='path to model 2')
    parser.add_argument('--topic', '-t',
                        type=int,
                        help='index of target topic')
    parser.add_argument('--binarize', '-b',
                        action='store_true',
                        default=False)
    parser.add_argument('--on_server',
                        action='store_true',
                        default=False)
    parser.add_argument('--ratio',
                        nargs="+",
                        type=int,
                        default=[20, 20, 40])
    parser.add_argument('--epochs',
                        type=int,
                        default=10,
                        help='number of epochs that averages(stabilizes) final result')
    args = parser.parse_args(argv)

    """
    E2E Training script for image, captions and comments
        by aggregating features with another LSTM
    """
    on_server = args.on_server
    topic = TOPICS[args.topic]
    print("Selected topic: {}".format(topic))
    num_classes = 2 if args.binarize else 5

    if on_server:
        src_path = '/pool1/users/ntomita/arra_2017/src/risk_predictor'
        image_path = '/pool1/users/ntomita/arradata_images_original/arra'
    else:
        src_path = '/home/ntomita/arra_2017/src/risk_predictor'
        image_path = '/home/ntomita/arradata_images_original/arra'
    mapping_path = join(src_path, 'vec_dict_0123.pickle')
    sids_path = join(src_path, 'data_sid.pickle')
    label_dic_path = join(src_path, 'data_label.pickle')
    split_dic_path = join(src_path, 'data_split.pickle')
    caption_dic_path = join(src_path, 'data_caption.pickle')
    comment_dic_path = join(src_path, 'data_comment.pickle')

    ratio = args.ratio

    mapping = load_wvmap(mapping_path)

    sids = pickle.load(open(sids_path, 'rb'))
    label_dic = pickle.load(open(label_dic_path, 'rb'))
    split_dic = pickle.load(open(split_dic_path, 'rb'))
    caption_dic = pickle.load(open(caption_dic_path, 'rb'))
    comment_dic = pickle.load(open(comment_dic_path, 'rb'))
    topic = 'survey_' + topic

    test_dataloader = MediaDataset(sids,
                                   label_dic,
                                   split_dic,
                                   image_path,
                                   caption_dic,
                                   comment_dic,
                                   ratio,
                                   mapping,
                                   topic,
                                   mode='test',
                                   binarize=args.binarize)

    test_dataloader.stats()

    """ net0: resnet (image feature extraction)
        net1: lstm (text feature extraction)
        net2: lstm (aggregation of image and text features)
    """
    net0 = torch.load(args.model0)
    net1 = torch.load(args.model1)
    net2 = torch.load(args.model2)

    criterion = CrossEntropyLoss()

    cuda = True

    if cuda:
        net0 = net0.cuda()
        net1 = net1.cuda()
        net2 = net2.cuda()
        criterion = criterion.cuda()

    num_epochs = args.epochs  # to average results
    pred_list = zeros(len(test_dataloader))
    label_list = zeros(len(test_dataloader))

    def validate(epoch):
        net0.train(False)
        net1.train(False)
        net2.train(False)
        reporter = Report(num_classes=num_classes)
        for i in range(len(test_dataloader)):
            batch = test_dataloader[i]
            images, caps, coms, label = batch
            label = label.type(LongTensor)
            images = Variable(images)
            h1 = net1.init_hidden(ratio[1])
            h2 = net1.init_hidden(ratio[2])
            if cuda:
                images = images.cuda()
                label = label.cuda()
                h1 = (h1[0].cuda(), h1[1].cuda())
                h2 = (h2[0].cuda(), h2[1].cuda())
            feat_imgs = net0(images)
            feat_caps = net1(caps, h1)
            feat_coms = net1(coms, h2)
            feats = cat([feat_imgs, feat_caps[-1], feat_coms[-1]], 0)
            feats = torch.mean(feats, dim=0)
            pred = net2(feats).view(1, -1)

            reporter.feed_softmax(pred, label)
            pred = nn.functional.softmax(pred.view(-1), dim=0)
            pred_list[i] += pred.data[1]
            label_list[i] += label.data[0]

            del images, feat_imgs, feat_caps, feat_coms, feats
            del h1, h2
            del pred
        print(reporter)
        return reporter.accuracy()

    def validate_temp(epoch):
        net0.train(False)
        net1.train(False)
        net2.train(False)
        reporter = Report(num_classes=num_classes)
        for i in range(len(test_dataloader)):
            batch = test_dataloader[i]
            _, caps, coms, label = batch
            label = label.type(LongTensor)
            h1 = net1.init_hidden(ratio[1])
            h2 = net1.init_hidden(ratio[2])
            if cuda:
                label = label.cuda()
                h1 = (h1[0].cuda(), h1[1].cuda())
                h2 = (h2[0].cuda(), h2[1].cuda())
            images_batch = test_dataloader.get_images_test(i)
            feat_imgs_list = list()
            for t in range(images_batch.size(0)):
                #print(t)
                images = images_batch[t]
                if cuda:
                    images = images.cuda()
                images = Variable(images)
                feat_imgs_crops = net0(images.detach())
                feat_imgs_avg = torch.mean(feat_imgs_crops.detach(), dim=0)
                feat_imgs_list.append(feat_imgs_avg)
                del images, feat_imgs_crops, feat_imgs_avg
            feat_imgs = torch.stack(feat_imgs_list)
            feat_caps = net1(caps, h1)
            feat_coms = net1(coms, h2)
            feats = cat([feat_imgs, feat_caps[-1], feat_coms[-1]], 0)
            feats = torch.mean(feats, dim=0)
            pred = net2(feats).view(1, -1)

            loss = criterion(pred, label)
            result = reporter.feed_softmax(pred, label)
            if label.data[0] == 1 and result > 0.6:
                # True Positive
                print("Loss: ", loss.data[0])
            pred = nn.functional.softmax(pred.view(-1), dim=0)
            pred_list[i] += pred.data[1]
            label_list[i] += label.data[0]

            del feat_imgs, feat_caps, feat_coms, feats
            del h1, h2
            del pred
        print(reporter)
        return reporter.accuracy()

    avg_accuracy = 0
    for epoch in range(1, num_epochs+1):
        sys.stdout.flush()
        acc = validate_temp(epoch)
        avg_accuracy += sum(acc) / len(acc)
    print("Avg Acc: {} over {} runs".format(avg_accuracy / num_epochs, num_epochs))
    pred_list /= num_epochs
    label_list /= num_epochs
    save_roc_curve(label_list, pred_list,
                   figdest='roc_curve_{}.png'.format(topic),
                   print_raw_measure=True)
    print("Averaged results")
    for i in range(len(pred_list)):
        k = test_dataloader.sids[i]
        if pred_list[i] > 0.5 and label_list[i] > 0.5:
            # TP
            print(k, pred_list[i])


if __name__ == '__main__':
    test()




