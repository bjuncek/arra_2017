import torch
from torch import nn, cat
from torch.nn.utils.rnn import pad_packed_sequence
from torch.autograd import Variable
from torchvision import models
from torch.nn.init import xavier_normal


def create_resnet(net_name='resnet18', use_pretrained=True,
                  finetune=True, out_size=300):
    if net_name == 'resnet18':
        net = models.resnet18(pretrained=use_pretrained)
        expansion = 1
    elif net_name == 'resnet34':
        net = models.resnet34(pretrained=use_pretrained)
        expansion = 1
    elif net_name == 'resnet50':
        net = models.resnet50(pretrained=use_pretrained)
        expansion = 4
    elif net_name == 'resnet101':
        net = models.resnet101(pretrained=use_pretrained)
        expansion = 4
    elif net_name == 'resnet152':
        net = models.resnet152(pretrained=use_pretrained)
        expansion = 4
    num_classes = 1000  # original output els of resnet
    net.fc = nn.Sequential(net.fc,
                           nn.BatchNorm1d(num_classes),
                           nn.ReLU(),
                           nn.Linear(num_classes, out_size))

    for p in net.parameters():
        p.required_grad = not finetune
    for p in net.fc.parameters():
        p.required_grad = True
    if not use_pretrained:
        for module in net.modules():
            if isinstance(module, nn.Conv2d):
                xavier_normal(module.weight)
    return net


class BiLSTMModel(nn.Module):
    def __init__(self, nlayers, nhid, in_size, out_size,
                 dropout=0, need_unpack=False):
        super(BiLSTMModel, self).__init__()
        self.nlayers = nlayers
        self.nhid = nhid
        self.in_size = in_size
        self.out_size = out_size

        self.lstm = nn.LSTM(in_size, nhid, nlayers, bidirectional=True)
        self.fc = nn.Linear(nhid*2, out_size)
        self.dropout = nn.Dropout(p=dropout)
        self.need_unpack = need_unpack

    def forward(self, x, hidden):
        out, _ = self.lstm(x, hidden)  # [seq_len, T, in_size] -> [seq_len, T, nhid]
        if self.need_unpack:
            out, _ = pad_packed_sequence(out)
        pred = self.fc(self.dropout(
            out.view(out.size(0)*out.size(1), out.size(2))))  # -> [seq_len*T, nhid*2] -> [seq_len*T, out_size]
        return pred.view(out.size(0), out.size(1), -1)  # -> [seq_len, T, out_size]

    def init_hidden(self, batch_size):
        return (Variable(torch.zeros([self.nlayers*2, batch_size, self.nhid])),
                Variable(torch.zeros([self.nlayers*2, batch_size, self.nhid])))


class LSTMModel(nn.Module):
    def __init__(self, nlayers, nhid, in_size, out_size,
                 dropout=0, need_unpack=False, bidirectional=False):
        super(LSTMModel, self).__init__()
        self.nlayers = nlayers
        self.nhid = nhid
        self.in_size = in_size
        self.out_size = out_size
        self.ndirections = 2 if bidirectional else 1

        self.lstm = nn.LSTM(in_size, nhid, nlayers, bidirectional=bidirectional)
        self.fc = nn.Linear(nhid*self.ndirections, out_size)
        self.dropout = nn.Dropout(p=dropout)
        self.need_unpack = need_unpack

    def forward(self, x, hidden):
        out, _ = self.lstm(x, hidden)  # [seq_len, T, in_size] -> [seq_len, T, nhid]
        if self.need_unpack:
            out, _ = pad_packed_sequence(out)
        pred = self.fc(self.dropout(
            out.view(out.size(0)*out.size(1), out.size(2))))  # -> [seq_len*T, nhid*2] -> [seq_len*T, out_size]
        return pred.view(out.size(0), out.size(1), -1)  # -> [seq_len, T, out_size]

    def init_hidden(self, batch_size):
        return (Variable(torch.zeros([self.nlayers*self.ndirections, batch_size, self.nhid])),
                Variable(torch.zeros([self.nlayers*self.ndirections, batch_size, self.nhid])))