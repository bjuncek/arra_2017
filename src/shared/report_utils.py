import matplotlib.pyplot as plt
import pickle


def show(filename, epoch_range=None):
    ax = pickle.load(file(filename, 'rb'))
    if epoch_range is not None:
        for axis in ax:
            axis.set_xlim(epoch_range)
    plt.show()
