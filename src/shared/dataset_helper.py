from torchvision.transforms import Compose, RandomCrop, ToTensor, Scale, Pad, CenterCrop, ToPILImage
from PIL import Image, ImageFile
from os import listdir, walk
from os.path import join, basename
from .transforms import _scale, Resize

MODE_NOTRANSFORM = 0
MODE_CENTERCROP = 1
MODE_SCALEDCROP = 2
MODE_ISOSCALE = 3

DAUG_RROTATE = 'random_rotate'
DAUG_HTRANS = 'horizontal_translate'
DAUG_HWARP = 'horizontal_warp'
DAUG_EDEFORM = 'elastic_deform'
DAUG_CCROP = 'center_crop'
DAUG_SCROP = 'scaled_crop'
DAUG_ISCALE = 'isotropic_scale'

ImageFile.LOAD_TRUNCATED_IMAGES = True

def get_mode(opts):
    mode = 0
    if 'data_aug' in opts:
        data_augs = opts['data_aug']
        if DAUG_CCROP in data_augs:
            mode = MODE_CENTERCROP
        elif DAUG_SCROP in data_augs:
            mode = MODE_SCALEDCROP
        elif DAUG_ISCALE in data_augs:
            mode = MODE_ISOSCALE
        else:
            print("Unrecognized mode")
    return mode


def compute_mean_ImageNet(list_files):
    f = Compose([Resize(224),
                 CenterCrop(224),
                 ToTensor()])
    tensor = None
    count = 0
    for file in list_files:
        im = Image.open(file)
        if tensor is None:
            tensor = f(im)
        else:
            tensor += f(im)
        del im
        count += 1
    return tensor / count

def compute_mean(list_files, mode=0):
    """
    mode
        0: no transformation
        1: center crop
        2: scaled center crop
    """
    if mode == MODE_NOTRANSFORM:
        f = ToTensor()
    elif mode == MODE_CENTERCROP:
        f = Compose([CenterCrop(224), ToTensor()])
    elif mode == MODE_SCALEDCROP:
        f = Compose([_scale(size=[512, 224]),
                     CenterCrop(224),
                     ToTensor()])
    elif mode == MODE_ISOSCALE:
        f = Compose([_scale(size=[224, 224]),
                     ToTensor()])
    tensor = None
    count = 0
    for file in list_files:
        im = Image.open(file)
        if tensor is None:
            tensor = f(im)
        else:
            tensor += f(im)
        count += 1
    return tensor / count


def is_image(path):
    try:
        return basename(path).split('.')[1].lower() in ['jpg', 'jpeg', 'png']
    except:
        return False


def get_images(root_dir):
    """
        returns a list containing paths of all images under a root_dir path
        If root_dir is a list, iterate over each dir in the list
    """
    images = []
    if isinstance(root_dir, list):
        if root_dir == []:
            return []
        else:
            return get_images(root_dir[0]) + get_images(root_dir[1:])

    for dirpath, dirs, files in walk(root_dir):
        for file in files:
            if is_image(file):
                images.append(join(dirpath, file))
    return images

