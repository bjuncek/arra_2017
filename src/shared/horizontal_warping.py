import numpy as np
from PIL import Image
import math


def quad_as_rect(quad):
    if quad[0] != quad[2]: return False
    if quad[1] != quad[7]: return False
    if quad[4] != quad[6]: return False
    if quad[3] != quad[5]: return False
    return True

def quad_to_rect(quad):
    assert(len(quad) == 8)
    assert(quad_as_rect(quad))
    return (quad[0], quad[1], quad[4], quad[3])

def rect_to_quad(rect):
    assert(len(rect) == 4)
    return (rect[0], rect[1], rect[0], rect[3], rect[2], rect[3], rect[2], rect[1])

def shape_to_rect(shape):
    assert(len(shape) == 2)
    return (0, 0, shape[0], shape[1])

def griddify(rect, w_div, h_div):
    # [height, width, channel] order
    w = rect[2] - rect[0]
    h = rect[3] - rect[1]
    x_step = w / float(w_div)
    y_step = h / float(h_div)
    y = rect[1]
    grid_vertex_matrix = []
    for _ in range(h_div + 1):
        grid_vertex_matrix.append([])
        x = rect[0]
        for _ in range(w_div + 1):
            grid_vertex_matrix[-1].append([int(x), int(y)])
            x += x_step
        y += y_step
    grid = np.array(grid_vertex_matrix)
    return grid

def distort_grid(org_grid, max_shift):
    new_grid = np.copy(org_grid)
    x_min = np.min(new_grid[:, :, 0])
    y_min = np.min(new_grid[:, :, 1])
    x_max = np.max(new_grid[:, :, 0])
    y_max = np.max(new_grid[:, :, 1])
    new_grid += np.random.randint(- max_shift, max_shift + 1, new_grid.shape)
    new_grid[:, :, 0] = np.maximum(x_min, new_grid[:, :, 0])
    new_grid[:, :, 1] = np.maximum(y_min, new_grid[:, :, 1])
    new_grid[:, :, 0] = np.minimum(x_max, new_grid[:, :, 0])
    new_grid[:, :, 1] = np.minimum(y_max, new_grid[:, :, 1])
    return new_grid

def hwarp_grid(org_grid, factor):
    new_grid = np.copy(org_grid)
    x_min = np.min(new_grid[:, :, 0])
    y_min = np.min(new_grid[:, :, 1])
    x_max = np.max(new_grid[:, :, 0])
    y_max = np.max(new_grid[:, :, 1])

    c_i = (new_grid.shape[0]-1) / 2.0
    c_j = (new_grid.shape[1]-1) / 2.0

    def distort(i, j, k):
        # i:h, j:w, k:[x,y]channel
        d = (1-k)  # only apply horizontal translation
        d = d * factor / c_j  # apply magnification factor
        d *= (c_j - j) * np.sqrt(np.abs(c_j - j))  # increase intensity as x away from center_x (sqrted)
        d = d * (3*c_i) / (np.abs(c_i - i) + 2*c_i)  # decrease intensity as y away from center_y
        return d.astype(int)

    displacement = np.fromfunction(lambda i, j, k: distort(i,j,k), new_grid.shape, dtype=int)
    new_grid += displacement
    new_grid[:, :, 0] = np.maximum(x_min, new_grid[:, :, 0])
    new_grid[:, :, 1] = np.maximum(y_min, new_grid[:, :, 1])
    new_grid[:, :, 0] = np.minimum(x_max, new_grid[:, :, 0])
    new_grid[:, :, 1] = np.minimum(y_max, new_grid[:, :, 1])
    return new_grid

def grid_to_mesh(src_grid, dst_grid):
    assert(src_grid.shape == dst_grid.shape)
    mesh = []
    for i in range(src_grid.shape[0] - 1):
        for j in range(src_grid.shape[1] - 1):
            src_quad = [src_grid[i    , j    , 0], src_grid[i    , j    , 1],
                        src_grid[i + 1, j    , 0], src_grid[i + 1, j    , 1],
                        src_grid[i + 1, j + 1, 0], src_grid[i + 1, j + 1, 1],
                        src_grid[i    , j + 1, 0], src_grid[i    , j + 1, 1]]
            dst_quad = [dst_grid[i    , j    , 0], dst_grid[i    , j    , 1],
                        dst_grid[i + 1, j    , 0], dst_grid[i + 1, j    , 1],
                        dst_grid[i + 1, j + 1, 0], dst_grid[i + 1, j + 1, 1],
                        dst_grid[i    , j + 1, 0], dst_grid[i    , j + 1, 1]]
            dst_rect = quad_to_rect(dst_quad)
            mesh.append([dst_rect, src_quad])
    return mesh

def horizontal_warping_test(input, dest='.', grid_size=16, min_factor=0, max_factor=10, step=1):
    from os.path import basename, join
    im = Image.open(input)
    for f in range(min_factor, max_factor+1, step):
        imt = horizontal_warp(im, grid_size, f)
        path = join(dest, 'warp_factor_{}.jpg'.format(f))
        imt.save(path, "JPEG")


def horizontal_warp(im, grid_size, factor):
    des_grid = griddify(shape_to_rect(im.size), grid_size, grid_size)
    src_grid = hwarp_grid(des_grid, factor)
    mesh = grid_to_mesh(src_grid, des_grid)
    imt = im.transform(im.size, Image.MESH, mesh, Image.BICUBIC)
    return imt


def main():
    im = Image.open('pos50.jpg')
    grid_size = 16
    imt = horizontal_warp(im, grid_size, 5.1)
    imt.show()



if __name__ == '__main__':
    main()
