from time import ctime, time
from torch import abs, pow, log
from torch.nn.functional import relu
import sys

"""
Random seed initializer
"""
def set_all_seeds(seed, cuda=False):
    import random
    random.seed(seed)
    import numpy
    numpy.random.seed(seed)
    import torch
    torch.manual_seed(seed)
    if cuda:
        torch.cuda.manual_seed(seed)


"""
Calculate differentiable True Positive Rate and Positive Predictive Value
    based on predictions
    Noted that no decision boundary applied
"""

EPS = sys.float_info.epsilon


def tpr_ppv(pred, label):
    pred = pred.squeeze()
    label = label.squeeze()
    tp = relu(label - 1 + pred)
    fn = relu(label - pred)
    fp = relu(pred - label)
    tn = relu(1 - label - pred)
    tpr = sum(tp) / sum(tp + fn).clamp(min=EPS)
    ppv = sum(tp) / sum(tp + fp).clamp(min=EPS)
    tnr = sum(tn) / sum(tn + fp).clamp(min=EPS)
    return [tpr, ppv, tnr]


def tpr_loss(pred, label, norm='l2'):
    assert norm in ['l1', 'l2', 'bce']
    tpr, _, _ = tpr_ppv(pred, label)
    if norm == 'l1':
        loss = abs(1 - tpr)
    elif norm == 'l2':
        loss = pow(1 - tpr, 2)
    elif norm == 'bce':
        loss = -log(tpr.clamp(min=EPS))
    return loss


def tnr_loss(pred, label, norm='l2'):
    assert norm in ['l1', 'l2', 'bce']
    _, _, tnr = tpr_ppv(pred, label)
    if norm == 'l1':
        loss = abs(1 - tnr)
    elif norm == 'l2':
        loss = pow(1 - trn, 2)
    elif norm == 'bce':
        loss = -log(tnr.clamp(min=EPS))
    return loss


def f1_loss(pred, label, norm='l2'):
    assert norm in ['l1', 'l2', 'bce']
    tpr, ppv, _ = tpr_ppv(pred, label)
    f1 = 2*ppv*tpr / (ppv + tpr)
    if norm == 'l1':
        loss = abs(1 - f1)
    elif norm == 'l2':
        loss = pow(1 - f1, 2)
    elif norm == 'bce':
        loss = -log(f1)
    return loss


def obj_loss(pred, label, objectives):
    """
    Wrapper function for TPR/F1 loss
    objectives: 'tpr_l1', 'tpr_l2', 'tpr_bce'
                'f1_l1', 'f1_l2', 'f1_bce'
    l1 uses L1 norm to calculate an error
    l2 uses L2 norm
    bce uses Binary Cross Entropy
    """
    loss = 0
    if objectives in ['tpr_l1', 'tpr_l2', 'tpr_bce']:
        mode = objectives[4:]
        loss = tpr_loss(pred, label, norm=mode)
    elif objectives in ['f1_l1', 'f1_l2', 'f1_bce']:
        mode = objectives[4:]
        loss = f1_loss(pred, label, norm=mode)
    return loss


"""
Utility Classes
"""


class SnapshotManager:
    def __init__(self, mode="min", every=10, ignore_first=10, window_size=20):
        """
        mode: mode of optimization, either min/max-mizing objectives
        every: save every {} epochs
        ignore_first: ignore first {} epochs as it may be noisy
        window_size: keep a snapshot that is best in recent n epochs. set 0 for global best
        """
        assert mode in ["min", "max"]
        self.mode = mode
        if mode == "min":
            self.best = float("Inf")
        else:
            self.best = -float("Inf")
        self.every = every
        self.ignore_first = ignore_first
        self.window_size = window_size
        self.window = list()

    def need_save(self, performance, epoch):
        self.window.append(performance)
        if len(self.window) > self.window_size:
            del self.window[0]
        # GLOBAL BEST
        is_better = (self.mode == 'min' and performance < self.best) or\
                    (self.mode == 'max' and performance > self.best)
        if is_better:
            self.best = performance
        # LOCAL BEST
        is_better = is_better or\
                    (self.mode == 'min' and performance == min(self.window)) or\
                    (self.mode == 'max' and performance == max(self.window))

        if epoch % self.every == 0 or (epoch > self.ignore_first and is_better):
            # EITHER GLOBAL OR LOCAL BEST
            return True
        else:
            return False


class ETC:
    """
    A class to estimate the time of completion for a training.
    Do tick() at the end of each epoch, usually at the end of validation 
    """
    def __init__(self, n_epoch, make_first_pred_at=2, make_pred_every=30):
        assert min(n_epoch, make_first_pred_at, make_pred_every) > 0
        self.n_epoch = n_epoch
        self.make_first_pred_at = make_first_pred_at
        self.make_pred_every = make_pred_every
        self.current_t = 0
        self.prev_time = time()
        self.sum_deltatime = 0.0

    def tick(self):
        curr_time = time()
        self.sum_deltatime += curr_time - self.prev_time
        self.prev_time = curr_time
        self.current_t += 1
        if self.current_t == self.make_first_pred_at or \
           self.current_t % self.make_pred_every == 0:
            avg_time_taken = self.sum_deltatime / self.current_t
            etc = avg_time_taken*(self.n_epoch-self.current_t) + self.prev_time
            print("INFO: Estimated Time of Completion (ETC): " + ctime(int(etc)))
