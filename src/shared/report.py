import torch
import sys
from collections import defaultdict


class Report:
    EPS = sys.float_info.epsilon
    TP_KEY = 0
    TN_KEY = 1
    FP_KEY = 2
    FN_KEY = 3

    def __init__(self, threshold=0.5):
        self.pos = 0
        self.neg = 0
        self.false_pos = 0
        self.false_neg = 0
        self.true_pos = 0
        self.true_neg = 0
        self.threshold = threshold
        self.pathdic = defaultdict(list)

    def feed(self, pred, label, paths=None):
        pred = (pred > self.threshold).float().squeeze()
        not_pred = (pred == 0).float().squeeze()
        label = label.squeeze()
        not_label = (label == 0).float().squeeze()

        self.pos += sum(label).data[0]
        self.neg += sum(not_label).data[0]

        fp = torch.dot(pred, not_label).data[0]
        self.false_pos += fp
        fn = torch.dot(not_pred, label).data[0]
        self.false_neg += fn
        tp = torch.dot(pred, label).data[0]
        self.true_pos += tp
        tn = torch.dot(not_pred, not_label).data[0]
        self.true_neg += tn

        feedback = pred*label*self.TP_KEY +\
            not_pred*not_label*self.TN_KEY +\
            pred*not_label*self.FP_KEY +\
            not_pred*label*self.FN_KEY

        if paths is not None:
            # Variable -> list of int
            feedback_int = [int(feedback.data[i]) for i in range(feedback.numel())]
            for i in range(len(feedback_int)):
                if feedback_int[i] == self.TP_KEY:
                    self.pathdic["TP"].append(paths[i])
                elif feedback_int[i] == self.TN_KEY:
                    self.pathdic["TN"].append(paths[i])
                elif feedback_int[i] == self.FP_KEY:
                    self.pathdic["FP"].append(paths[i])
                elif feedback_int[i] == self.FN_KEY:
                    self.pathdic["FN"].append(paths[i])

        return feedback

    def stats(self):
        text = ("Total Positives: {}".format(self.pos),
                "Total Negatives: {}".format(self.neg),
                "Total TruePos: {}".format(self.true_pos),
                "Total TrueNeg: {}".format(self.true_neg),
                "Total FalsePos: {}".format(self.false_pos),
                "Total FalseNeg: {}".format(self.false_neg))
        return "\n".join(text)

    def accuracy(self):
        return (self.true_pos+self.true_neg) / max((self.pos+self.neg), self.EPS)

    def __summarize(self):
        assert self.pos > 0 and self.neg > 0
        self.ACC = self.accuracy()

        self.P_TPR = self.true_pos / max(self.pos, self.EPS)
        self.P_PPV = self.true_pos / max((self.true_pos + self.false_pos), self.EPS)
        self.P_F1 = 2*self.true_pos / max((2*self.true_pos + self.false_pos + self.false_neg), self.EPS)

        self.N_TPR = self.true_neg / max(self.neg, self.EPS)
        self.N_PPV = self.true_neg / max((self.true_neg + self.false_neg), self.EPS)
        self.N_F1 = 2*self.true_neg / max((2*self.true_neg + self.false_neg + self.false_pos), self.EPS)

    def __str__(self):
        self.__summarize()
        summary = ("Accuracy: {:.4f}".format(self.ACC),
                   "For fractured class:",
                   "TP(sensitivity,recall): {:.4f}".format(self.P_TPR),
                   "PPV(precision): {:.4f}".format(self.P_PPV),
                   "F-1: {:.4f}".format(self.P_F1),
                   "",
                   "For normal class:",
                   "TP(sensitivity,recall): {:.4f}".format(self.N_TPR),
                   "PPV(precision): {:.4f}".format(self.N_PPV),
                   "F-1: {:.4f}".format(self.N_F1))
        return "\n".join(summary)
