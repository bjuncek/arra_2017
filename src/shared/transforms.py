from torchvision.transforms import Compose, RandomCrop, ToTensor, Scale, Pad, CenterCrop, ToPILImage

from PIL import Image
from .elastic_transform import elastic_transform
from .horizontal_warping import horizontal_warp
from numpy import array
from random import uniform, randint, random
# from dataset_helper import DAUG_RROTATE, DAUG_HTRANS, DAUG_HWARP, DAUG_EDEFORM, DAUG_CCROP, DAUG_SCROP


### FROM LATEST VISION CODE

def _is_pil_image(img):
    return isinstance(img, Image.Image)

def resize(img, size, interpolation=Image.BILINEAR):
    """Resize the input PIL.Image to the given size.
    Args:
        img (PIL.Image): Image to be resized.
        size (sequence or int): Desired output size. If size is a sequence like
            (h, w), the output size will be matched to this. If size is an int,
            the smaller edge of the image will be matched to this number maintaing
            the aspect ratio. i.e, if height > width, then image will be rescaled to
            (size * height / width, size)
        interpolation (int, optional): Desired interpolation. Default is
            ``PIL.Image.BILINEAR``
    Returns:
        PIL.Image: Resized image.
    """
    if not _is_pil_image(img):
        raise TypeError('img should be PIL Image. Got {}'.format(type(img)))
    if not (isinstance(size, int) or (isinstance(size, collections.Iterable) and len(size) == 2)):
        raise TypeError('Got inappropriate size arg: {}'.format(size))

    if isinstance(size, int):
        w, h = img.size
        if (w <= h and w == size) or (h <= w and h == size):
            return img
        if w < h:
            ow = size
            oh = int(size * h / w)
            return img.resize((ow, oh), interpolation)
        else:
            oh = size
            ow = int(size * w / h)
            return img.resize((ow, oh), interpolation)
    else:
        return img.resize(size[::-1], interpolation)

class Resize(object):
    """Resize the input PIL.Image to the given size.
    Args:
        size (sequence or int): Desired output size. If size is a sequence like
            (h, w), output size will be matched to this. If size is an int,
            smaller edge of the image will be matched to this number.
            i.e, if height > width, then image will be rescaled to
            (size * height / width, size)
        interpolation (int, optional): Desired interpolation. Default is
            ``PIL.Image.BILINEAR``
    """

    def __init__(self, size, interpolation=Image.BILINEAR):
        assert isinstance(size, int) or (isinstance(size, collections.Iterable) and len(size) == 2)
        self.size = size
        self.interpolation = interpolation

    def __call__(self, img):
        """
        Args:
            img (PIL.Image): Image to be scaled.
        Returns:
            PIL.Image: Rescaled image.
        """
        return resize(img, self.size, self.interpolation)


def five_crop(img, size):
    """Crop the given PIL.Image into four corners and the central crop.
    Note: this transform returns a tuple of images and there may be a mismatch in the number of
    inputs and targets your `Dataset` returns.
    Args:
       size (sequence or int): Desired output size of the crop. If size is an
           int instead of sequence like (h, w), a square crop (size, size) is
           made.
    Returns:
        tuple: tuple (tl, tr, bl, br, center) corresponding top left,
            top right, bottom left, bottom right and center crop.
    """
    if isinstance(size, numbers.Number):
        size = (int(size), int(size))
    else:
        assert len(size) == 2, "Please provide only two dimensions (h, w) for size."

    w, h = img.size
    crop_h, crop_w = size
    if crop_w > w or crop_h > h:
        raise ValueError("Requested crop size {} is bigger than input size {}".format(size,
                                                                                      (h, w)))
    tl = img.crop((0, 0, crop_w, crop_h))
    tr = img.crop((w - crop_w, 0, w, crop_h))
    bl = img.crop((0, h - crop_h, crop_w, h))
    br = img.crop((w - crop_w, h - crop_h, w, h))
    center = CenterCrop((crop_h, crop_w))(img)
    return (tl, tr, bl, br, center)


def ten_crop(img, size, vertical_flip=False):
    """Crop the given PIL.Image into four corners and the central crop plus the
       flipped version of these (horizontal flipping is used by default).
       Note: this transform returns a tuple of images and there may be a mismatch in the number of
       inputs and targets your `Dataset` returns.
       Args:
           size (sequence or int): Desired output size of the crop. If size is an
               int instead of sequence like (h, w), a square crop (size, size) is
               made.
           vertical_flip (bool): Use vertical flipping instead of horizontal
        Returns:
            tuple: tuple (tl, tr, bl, br, center, tl_flip, tr_flip, bl_flip,
                br_flip, center_flip) corresponding top left, top right,
                bottom left, bottom right and center crop and same for the
                flipped image.
    """
    if isinstance(size, numbers.Number):
        size = (int(size), int(size))
    else:
        assert len(size) == 2, "Please provide only two dimensions (h, w) for size."

    first_five = five_crop(img, size)

    if vertical_flip:
        img = vflip(img)
    else:
        img = hflip(img)

    second_five = five_crop(img, size)
    return first_five + second_five


class TenCrop(object):
    """Crop the given PIL.Image into four corners and the central crop plus the
       flipped version of these (horizontal flipping is used by default)
       Note: this transform returns a tuple of images and there may be a mismatch in the number of
       inputs and targets your `Dataset` returns.
       Args:
           size (sequence or int): Desired output size of the crop. If size is an
               int instead of sequence like (h, w), a square crop (size, size) is
               made.
           vertical_flip(bool): Use vertical flipping instead of horizontal
    """

    def __init__(self, size, vertical_flip=False):
        self.size = size
        if isinstance(size, numbers.Number):
            self.size = (int(size), int(size))
        else:
            assert len(size) == 2, "Please provide only two dimensions (h, w) for size."
            self.size = size
        self.vertical_flip = vertical_flip

    def __call__(self, img):
        return ten_crop(img, self.size, self.vertical_flip)

### END FROM LATEST VISION CODE


def RandomRotate(degree):
    def rotate(im):
        d = uniform(-degree, degree)
        return im.rotate(d, resample=Image.BICUBIC)
    return rotate


def ElasticDeform(alpha=(3, 6)):
    """Randomized
    """
    def deform(im):
        im_sk = array(im)
        a = uniform(alpha[0], alpha[1])
        s = uniform(a, 2*a)
        return Image.fromarray(elastic_transform(im_sk, alpha=a, sigma=s))
    return deform


def HorizontalWarp(factor=(-3, 6)):
    """Randomized
    """
    def deform(im):
        f = uniform(factor[0], factor[1])
        return horizontal_warp(im, grid_size=8, factor=f)
    return deform


class VolumeTransformer:
    """ tranformation generator for C3D/Sequence
    A generated tranformation is applied on all slices in a volume

    Available Transformations:
    ['random_rotate',
      'horizontal_translation',
      'horizontal_warping',
      'elastic_transform',
      'center_crop' | 'scaled_crop' | 'isotropic_scale']

    """
    def __init__(self, size, ht, rr, ed, hw, cc, sc, isos, test=False):
        """

        Keyword arguments:
        size -- the input image size in tuple (width, height)
                (Required)
        ht   -- the param for horizontal translation (recommended value: 0.05)
                (None if don't use)
        rr   -- the param for random rotation (recommended value: 3)
                (None if don't use)
        ed   -- the params for elastic deformation (recommended value: (3,6))
                (None if don't use)
        hw   -- the param for horizontal warping (recommended value: (-3,6))
                (None if don't use)
        cc   -- the size of output if use square center cropping (MutEx: sc, is)
        sc   -- the size of output if use square scaled cropping (MutEx: cc, is)
        isos   -- the size of output if use isotropic scaling (MutEx: cc, sc)
        test -- True if testing mode
                (Default: False)

        """
        self.width = size[0]
        self.height = size[1]
        self.ht = ht
        self.rr = rr
        self.ed = ed
        self.hw = hw
        self.cc = cc
        self.sc = sc
        self.isos = isos
        self.test = test

    def generate(self):
        data_aug = []
        if self.test:
            if self.cc is not None:
                # Center Cropping
                data_aug.append(CenterCrop(self.cc))
            elif self.sc is not None:
                # Scaled Cropping
                data_aug.append(_scale(size=[self.width, self.sc]))
                data_aug.append(CenterCrop(self.sc))
            elif self.isos is not None:
                data_aug.append(_scale(size=[self.isos, self.isos]))
            data_aug.append(ToTensor())
            return Compose(data_aug)

        if self.ht is not None:
            # Horizontal Translation
            pad = int(self.width*self.ht)
            data_aug.append(Pad(pad, 0))
            left = randint(0, 2*pad)
            data_aug.append(Crop(box=[left, 0, left+self.width, self.height]))
        if self.rr is not None:
            # Random Rotation
            r = uniform(-self.rr, self.rr)
            data_aug.append(Rotate(r))
        if self.ed is not None:
            # Elastic Deformation
            alpha = uniform(self.ed[0], self.ed[1])
            sigma = uniform(alpha, 2*alpha)
            data_aug.append(Deform(alpha, sigma))
        if self.hw is not None:
            factor = uniform(self.hw[0], self.hw[1])
            data_aug.append(HWarp(factor))
        if self.cc is not None:
            # Center Cropping
            data_aug.append(CenterCrop(self.cc))
        elif self.sc is not None:
            # Scaled Cropping
            data_aug.append(_scale(size=[self.width, self.sc]))
            data_aug.append(CenterCrop(self.sc))
        elif self.isos is not None:
            # Isotropic scaling
            data_aug.append(_scale(size=[self.isos, self.isos]))

        # ToTensor
        data_aug.append(ToTensor())
        return Compose(data_aug)

""" deterministic data augs used for VolumeTransformer
"""


def _scale(size):
    def resize(im):
        return im.resize(size, Image.BILINEAR)
    return resize


def Rotate(degree):
    def rotate(im):
        return im.rotate(degree, resample=Image.BICUBIC)
    return rotate


def Crop(box):
    # box:[left, upper, right, lower]
    def crop(im):
        return im.crop(box)
    return crop


def Deform(alpha, sigma):
    def deform(im):
        return Image.fromarray(elastic_transform(array(im), alpha=alpha, sigma=sigma))
    return deform


def HWarp(factor):
    def hwarp(im):
        return horizontal_warp(im, grid_size=8, factor=factor)
    return hwarp
