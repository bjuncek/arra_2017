#!/usr/bin/bash

mkdir -p /data/arra/dataset_survey_tobacco/val
mkdir -p /data/arra/dataset_survey_tobacco/val/0
mkdir -p /data/arra/dataset_survey_tobacco/val/1
mkdir -p /data/arra/dataset_survey_tobacco/val/2
mkdir -p /data/arra/dataset_survey_tobacco/val/3
mkdir -p /data/arra/dataset_survey_tobacco/val/4
mkdir -p /data/arra/dataset_survey_tobacco/val/5
mkdir -p /data/arra/dataset_survey_drinks/val
mkdir -p /data/arra/dataset_survey_drinks/val/0
mkdir -p /data/arra/dataset_survey_drinks/val/1
mkdir -p /data/arra/dataset_survey_drinks/val/2
mkdir -p /data/arra/dataset_survey_drinks/val/3
mkdir -p /data/arra/dataset_survey_drinks/val/4
mkdir -p /data/arra/dataset_survey_drinks/val/5
mkdir -p /data/arra/dataset_survey_illegal_drugs/val
mkdir -p /data/arra/dataset_survey_illegal_drugs/val/0
mkdir -p /data/arra/dataset_survey_illegal_drugs/val/1
mkdir -p /data/arra/dataset_survey_illegal_drugs/val/2
mkdir -p /data/arra/dataset_survey_illegal_drugs/val/3
mkdir -p /data/arra/dataset_survey_illegal_drugs/val/4
mkdir -p /data/arra/dataset_survey_illegal_drugs/val/5



for file in /data/arra/dataset_survey_tobacco/train/0/*.jpeg; do
  val=$RANDOM
  
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_tobacco/val/0/
  fi
done

for file in /data/arra/dataset_survey_tobacco/train/1/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_tobacco/val/1/
  fi
done
for file in /data/arra/dataset_survey_tobacco/train/2/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_tobacco/2/neg/
  fi
done
for file in /data/arra/dataset_survey_tobacco/train/3/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_tobacco/val/3/
  fi
done
for file in /data/arra/dataset_survey_tobacco/train/4/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_tobacco/val/4/
  fi
done
for file in /data/arra/dataset_survey_tobacco/train/5/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_tobacco/val/5/
  fi
done


for file in /data/arra/dataset_survey_illegal_drugs/train/0/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_illegal_drugs/val/0/
  fi
done
for file in /data/arra/dataset_survey_illegal_drugs/train/1/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_illegal_drugs/val/1/
  fi
done
for file in /data/arra/dataset_survey_illegal_drugs/train/2/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_illegal_drugs/val/2/
  fi
done
for file in /data/arra/dataset_survey_illegal_drugs/train/3/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_illegal_drugs/val/3/
  fi
done
for file in /data/arra/dataset_survey_illegal_drugs/train/4/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_illegal_drugs/val/4/
  fi
done
for file in /data/arra/dataset_survey_illegal_drugs/train/5/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_illegal_drugs/val/5/
  fi
done


for file in /data/arra/dataset_survey_drinks/train/5/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_drinks/val/5/
  fi
done
for file in /data/arra/dataset_survey_drinks/train/4/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_drinks/val/4/
  fi
done
for file in /data/arra/dataset_survey_drinks/train/3/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_drinks/val/3/
  fi
done
for file in /data/arra/dataset_survey_drinks/train/2/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_drinks/val/2/
  fi
done
for file in /data/arra/dataset_survey_drinks/train/1/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_drinks/val/1/
  fi
done
for file in /data/arra/dataset_survey_drinks/train/0/*.jpeg; do
  val=$RANDOM
  if [ $val -le 3276 ]; then
    mv "$file" -vt /data/arra/dataset_survey_drinks/val/0/
  fi
done