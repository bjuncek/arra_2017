from collections import defaultdict
from pandas import read_csv
import pickle

# topics = ['survey_age',
#           'survey_gender',
#           'survey_race',
#           'survey_height',
#           'survey_weight',
#           'survey_drinks',
#           'survey_tobacco',
#           'survey_illegal_drugs',
#           'survey_prescription_drugs',
#           'survey_breakup',
#           'survey_creativity',
#           'survey_death_loved',
#           'survey_food',
#           'survey_happy',
#           'survey_major_friendship',
#           'survey_narcissist',
#           'survey_other_loss']
topics_ind = list(range(2, 2+len(topics)+1))
path_user_data = '/home/ntomita/arradata/user_data.csv'
path_output = 'userdata.pickle'

user_dic = read_csv(path_user_data)
label_dic = defaultdict(dict)
topics = user_dic.keys()
for rind, row in user_dic.iterrows():
    uid = str(int(row[0]))
    for cind, topic in enumerate(topics):
        if cind == 0:
            continue
        try:
            label_dic[uid][topic] = int(row[cind])
        except:
            pass
pickle.dump(label_dic, open(path_output, 'wb'))
