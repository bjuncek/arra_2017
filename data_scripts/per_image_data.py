#!/usr/bin/python2.7

import shutil
import sys, os

import pandas as pd


tags = ["survey_drinks","survey_tobacco", "survey_illegal_drugs", "survey_prescription_drugs"]


def get_immediate_subdirectories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

# Main - just trying to be consistent with python specs
def main(argv):
    # read in the user info
    print("Reading in user_data.csv file...")
    arradata = pd.read_csv('user_data.csv')

    # for each subdirectory in the folder
    dirs_raw = get_immediate_subdirectories('.')
    dirs = [dir for dir in dirs_raw if not "dataset" in dir \
                                    or not "emojis" in dir \
                                    or not "__pycache__"]
    print(dirs)
    # create subdirectory for a complete dataset if it doesn't exist
    for tag in tags:
        datadir = "dataset_" + tag
        if not os.path.exists(datadir):
            os.makedirs(datadir)
            os.makedirs(datadir+'/train')
            os.makedirs(datadir+'/train/0')
            os.makedirs(datadir+'/train/1')
            os.makedirs(datadir+'/train/2')
            os.makedirs(datadir+'/train/3')
            os.makedirs(datadir+'/train/4')
            os.makedirs(datadir+'/train/5')
        count = 0
        for d in dirs:
            # get user id-s
            if d == 'emojis' or d == '__pycache__' or d == '.git': continue
            ind = arradata['Unnamed: 0'] == int(d)
            temp = arradata[ind]
            try:
                temp_bool = temp[tag] == 0
                if temp_bool.as_matrix()[0] == True:  
                    src = os.listdir(d)                                                                               
                    for f in src:                                                                                     
                        shutil.copy('./' + d + '/' + f, './' + datadir+'/train/0/'+ f + '.jpeg')                    
                        count= count + 1; 
                temp_bool = temp[tag] == 1    
                if temp_bool.as_matrix()[0] == True:               
                    src = os.listdir(d)                                                     
                    for f in src:        
                        shutil.copy('./' + d + '/' + f, './' + datadir+'/train/1/'+ f + '.jpeg')     
                        count= count + 1; 
                temp_bool = temp[tag] == 2
                if temp_bool.as_matrix()[0] == True:         
                    src = os.listdir(d)                 
                    for f in src:                                                                   
                        shutil.copy('./' + d + '/' + f, './' + datadir+'/train/2/'+ f + '.jpeg')    
                        count= count + 1;
                temp_bool = temp[tag] == 3
                if temp_bool.as_matrix()[0] == True:                                                          
                    src = os.listdir(d)                                                                       
                    for f in src:                                                                             
                        shutil.copy('./' + d + '/' + f, './' + datadir+'/train/3/'+ f + '.jpeg')              
                        count= count + 1;
                temp_bool = temp[tag] == 4
                if temp_bool.as_matrix()[0] == True:                                                          
                    src = os.listdir(d)                                                                       
                    for f in src:                                                                             
                        shutil.copy('./' + d + '/' + f, './' + datadir+'/train/4/'+ f + '.jpeg')              
                        count= count + 1;
                temp_bool = temp[tag] == 5
                if temp_bool.as_matrix()[0] == True:                                                          
                    src = os.listdir(d)                                                                       
                    for f in src:                                                                             
                        shutil.copy('./' + d + '/' + f, './' + datadir+'/train/5/'+ f + '.jpeg')              
                        count= count + 1; 
                
            except IndexError:
                continue

        print(count)


if __name__ == '__main__':
    main(sys.argv)