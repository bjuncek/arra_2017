import os
from os.path import join
from pandas import read_csv
import numpy as np
import csv

"""
Based on the result of face detection process,
update an image_metadata csv with the number of face
detected in each image
This update creates a new csv file and don't overwrite
the original metadata file.
"""

OPENFACE_ROOT = '/home/ntomita/OpenFace'
METAFILE_ROOT = '/media/ntomita/Data/#ARRA project (all files)/arradata_master/'
original_image_metadata_path = join(METAFILE_ROOT, 'clean_image_metadata.csv')
extended_image_metadata_path = join(METAFILE_ROOT, 'clean_image_metadata_facecount.csv')

output = open(extended_image_metadata_path, 'w', newline='')
writer = csv.writer(output, delimiter=',')

processed = join(OPENFACE_ROOT, 'processed')
meta_data = read_csv(original_image_metadata_path, dtype=str)
writer.writerow(list(meta_data)+["face_count"])
for rind, row in enumerate(meta_data.itertuples()):
    row = [r if r is not None and r is not np.nan else "" for r in row]  # convert to list
    row = row[1:]
    image_id = row[7]
    if len(image_id) < 1:
        # no image id
        continue
    image_file_name = str(int(image_id))
    report_file = join(processed, '{}.csv'.format(image_file_name))
    num_faces = 0
    # if os.exists(report_file):
    #     # at least one face detected
    #     pass
    #     f = open(report_file, 'r')
    #     num_faces = len(list(f)) - 1
    row.append(str(num_faces))
    writer.writerow(row)
