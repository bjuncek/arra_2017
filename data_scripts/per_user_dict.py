#!/usr/bin/python2.7

import shutil
import sys, os

import pickle
import pandas as pd



tags = ["survey_drinks","survey_tobacco", "survey_illegal_drugs", "survey_prescription_drugs"]

def get_immediate_subdirectories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

# Main - just trying to be consistent with python specs
def main(argv):
    # read in the user info
    print("Reading in user_data.csv file...")
    arradata = pd.read_csv('/data/arra/user_data.csv')

    # for each subdirectory in the folder
    dirs_raw = get_immediate_subdirectories('/data/arra')
    
    dirs_rm = [dir for dir in dirs_raw if "dataset" in dir \
                                    or "emojis" in dir \
                                    or "__pycache__" in dir]
    dirs = [dir for dir in dirs_raw if dir not in dirs_rm]
    #print(dirs)

    return_dict = {}
    for d in dirs:
        return_dict[d]={}
    # Create dictionary
    for tag in tags:
        for d in dirs:
            # get user id-s
            if d == 'emojis' or d == '__pycache__' or d == '.git': continue
            ind = arradata['Unnamed: 0'] == int(d)
            temp = arradata[ind]
            try:
                temp_bool = temp[tag] == 0
                if temp_bool.as_matrix()[0] == True:  
                    return_dict[d][tag]=0
                temp_bool = temp[tag] == 1    
                if temp_bool.as_matrix()[0] == True:               
                    return_dict[d][tag]=1 
                temp_bool = temp[tag] == 2
                if temp_bool.as_matrix()[0] == True:         
                    return_dict[d][tag]=2
                temp_bool = temp[tag] == 3
                if temp_bool.as_matrix()[0] == True:                                                          
                    return_dict[d][tag]=3
                temp_bool = temp[tag] == 4
                if temp_bool.as_matrix()[0] == True:                                                          
                    return_dict[d][tag]=4
                temp_bool = temp[tag] == 5
                if temp_bool.as_matrix()[0] == True:                                                          
                    return_dict[d][tag]=5 
                
            except IndexError:
                continue


    pickle.dump(return_dict, open( "dict.p", "wb" ))


if __name__ == '__main__':
    main(sys.argv)