import pickle
from os.path import join, dirname, basename
from os import walk, makedirs
from shutil import copy2

# pickled dictionary when creating a split on cap/com data
datasplit_dict_path = '/home/ntomita/rnn/extract_arra_data/datasplit_dict.pickle'
images_path = '/home/ntomita/master_dataset'
copy_destination = '/home/ntomita/master_dataset_v2'
datasplit_labels = ['train', 'val', 'test']
conversion_table = {'train': 'train', 'valid': 'val', 'test': 'test'}
d = pickle.load(open(datasplit_dict_path, 'rb'))

for dirpath, dirs, files in walk(images_path):
    sid = basename(dirpath)
    if sid in d:
        split = d[sid]
        split = conversion_table[split]
        target_path = join(copy_destination, split, sid)
        for file in files:
            # to make sure the current dirpath has (image) files
            makedirs(target_path, exist_ok=True)
            copy2(join(dirpath, file), target_path)

