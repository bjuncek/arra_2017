import os
from os.path import join
from pandas import read_csv
import numpy as np
import csv

ALCOHOL_RELATED_TERMS = \
    ['alchohol',
     'Alcohol',
     'alochol',
     'alcoholic_beverages',
     'alchol',
     'booze',
     'alcoholic_beverage',
     'alcoholic_drinks',
     'intoxicating_liquor',
     'drink',
     'liquor',
     'intoxicating_beverages',
     'underage_drinking',
     'drinking',
     'binge_drinking']


METAFILE_ROOT = '/media/ntomita/Data/#ARRA project (all files)/arradata_master/'
original_image_metadata_path = join(METAFILE_ROOT, 'clean_image_metadata.csv')
extended_image_metadata_path = join(METAFILE_ROOT, 'clean_image_metadata_alcoholcount.csv')

output = open(extended_image_metadata_path, 'w', newline='')
writer = csv.writer(output, delimiter=',', quoting=csv.QUOTE_NONNUMERIC)

meta_data = read_csv(original_image_metadata_path, dtype=str)
writer.writerow(list(meta_data)+["alcohol_count_comment", "alcohol_count_caption"])
for rind, row in enumerate(meta_data.itertuples()):
    row = [r if r is not None and r is not np.nan else "" for r in row]
    row = row[1:]
    image_id = row[7]
    if len(image_id) < 1:
        # no image id
        continue
    image_com = row[4]
    image_cap = row[5]
    num_terms_comment = 0
    num_terms_caption = 0
    for t in ALCOHOL_RELATED_TERMS:
        num_terms_comment += image_com.count(t)
        num_terms_caption += image_cap.count(t)

    row.append(str(num_terms_comment))
    row.append(str(num_terms_caption))
    writer.writerow(row)
