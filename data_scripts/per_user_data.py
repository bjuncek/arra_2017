#!/usr/bin/python2.7

import shutil, errno
import sys, os
import random

import pandas as pd


tags = ["survey_drinks","survey_tobacco", "survey_illegal_drugs", "survey_prescription_drugs"]


def recursive_copy(src, dst):
    print("Copy from " + src + " to " +dst)
    try:
        shutil.copytree(src, dst)
    except OSError as exc: # python >2.5
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
        else: raise


def copy_dir(dirlist, root, dst):
    for d in dirlist:
        src = root+d+''
        dest = dst+'/'+d
        recursive_copy(src, dest)



def get_immediate_subdirectories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

# Main - just trying to be consistent with python specs
def main(argv):
    # read in the user info
    print("Reading in user_data.csv file...")
    arradata = pd.read_csv('/data/arra/user_data.csv')

    # for each subdirectory in the folder
    dirs_raw = get_immediate_subdirectories('/data/arra')
    
    dirs_rm = [dir for dir in dirs_raw if "dataset" in dir \
                                    or "emojis" in dir \
                                    or "__pycache__" in dir \
                                    or ".git" in dir]

    print("Directories ignored:")
    print(dirs_rm)

    dirs = [dir for dir in dirs_raw if dir not in dirs_rm]
    
# create subdirectory for a complete dataset if it doesn't exist
    datadir = "/pool1/arra/master_dataset"
    print("Create folder at: " + datadir)

    if not os.path.exists(datadir):
        os.makedirs(datadir)
        os.makedirs(datadir+'/train')
        os.makedirs(datadir+'/val')
        os.makedirs(datadir+'/test')

    # 10% goes to test
    test_dirs = random.sample(dirs, int(len(dirs)*0.1))
    dirs = [d for d in dirs if d not in test_dirs]
    # 10% goes to val
    val_dirs = random.sample(dirs, int(len(dirs)*0.1))
    # rest to train
    train_dirs = [d for d in dirs if d not in val_dirs]


    copy_dir(val_dirs, "/data/arra/", datadir+'/val')
    copy_dir(test_dirs, "/data/arra/", datadir+'/test')
    copy_dir(train_dirs, "/data/arra/", datadir+'/train')



    print(count)


if __name__ == '__main__':
    main(sys.argv)