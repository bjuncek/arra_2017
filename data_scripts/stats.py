from os import walk
from os.path import basename, join, dirname
import pickle
from collections import Counter, defaultdict


def image_stats():
    target_path = '/home/ntomita/master_dataset'
    num_dirs = 0
    total_files = 0
    for dirpath, dirs, files in walk(target_path):
        if len(files) > 0:
            num_dirs += 1
            total_files += len(files)
    print("Image Stats:")
    print("#Dirs:", num_dirs)
    print("#Files:", total_files)
    print("Avg #Files:", total_files / num_dirs)


def label_stats():
    split_data_path = '/home/ntomita/arra_2017/src/risk_predictor/data_split.pickle'
    label_data_path = '/home/ntomita/arra_2017/src/risk_predictor/data_label.pickle'
    sid_data_path = '/home/ntomita/arra_2017/src/risk_predictor/data_sid.pickle'
    split_d = pickle.load(open(split_data_path, 'rb'))
    sids = pickle.load(open(sid_data_path, 'rb'))
    label_d = pickle.load(open(label_data_path, 'rb'))
    d = defaultdict(Counter)
    for sid in sids:
        for topic in label_d[sid].keys():
            d[(split_d[sid], topic)][label_d[sid][topic]] += 1
    for k in d:
        print(k, d[k])
    return d


